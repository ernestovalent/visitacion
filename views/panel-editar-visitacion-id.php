<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <?php include "links/head.php" ?>
  <title>Editar visitación registrada por ID</title>
</head>
<?php
session_start();
if (!$_SESSION["validar"]) {
  header("location:index.php?v=iniciar");
  exit();
}
?>
<?php
$visitacionId = new MainController();

foreach ($visitacionId->CRUDVistaVisitacionIdController($_GET['id']) as $key => $v) {
  $idregistro = $v['idregistro'];
  $fecha = $v['fecha'];
  $idcaseta = $v['idcaseta'];
  $caseta = $v['caseta'];
  $visitantes = $v['visitantes'];
  $nacionalidad = $v['nacionalidad'];
}
$visitacionId->CRUDActualizarVisitacionIDController($idregistro, $_GET['id']);
?>
<body>
  <style media="screen">
  .btn{
    width: 100%;
  }
  @media only screen and (min-width : 601px) {

  }
  @media only screen and (min-width : 993px) {

  }
  @media only screen and (min-width : 1201px) {

  }
</style>
<header>
  <div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper container">
        <a href="?panel=editar-visitacion" class="left"><i class="material-icons" style="font-size:3rem;margin-right:2rem;">arrow_back</i></a>
        <a href="#titulo" class="brand-logo">Editar Visitación</a>
    </div>
  </nav>
</div>
</header>

<main class="container">
    <!-- Visitacion -->
    <div class="card-panel" id="divVisitacion">
      <form method="post">
        <div class="row">
          <div class="col s12 m6">
            <p class="flow-text"><?php echo "Registro número ".$idregistro.", fila ".$_GET['id']; ?>
            </p>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12 m5">
            <input type="date" name="fecha" id="fecha" class="datepicker" data-value="<?php echo $fecha; ?>" required>
            <label for="fecha">Fecha</label>
          </div>
          <!--
          <div class="col s12 m6">
            <a class="btn-floating btn-flat waves-effect waves-light erase" id="" onclick=""><i class="material-icons">delete</i></a>
          </div>-->
        </div>
        <div class="row">
          <div class="input-field col s12 m5">
            <select name="caseta" id="caseta">
              <option value="" disabled selected><?php echo $caseta;  ?></option>
              <?php
              foreach ($visitacionId->CRUDVistaCasetasController() as $key => $v) {
                ?>
                <option value="<?php echo $v['idcasetas'];?>"><?php echo $v['estacion']." - ".$v['nombre']; ?></option>
                <?php
              }
               ?>
            </select>
            <label for="caseta">Caseta</label>
          </div>
          <div class="input-field col s12 m7">
            <select disabled multiple name="guardaparques[]" id="guardaparques">
              <option value="" disabled selected><?php foreach ($visitacionId->CRUDVistaGuardaparquesVisitacionController($idregistro) as $key => $v) {
                echo $v['nombre'].", ";
              } ?></option>
              <?php foreach ($visitacionId->CRUDVistaGuardaparquesController() as $key => $v) {
                ?>
                <option value="<?php echo $value['idguardaparques']; ?>"><?php echo $v['nombre']." ".$v['apellido']; ?></option>
                <?php
              } ?>
            </select>
            <label for="guardaparques">Guardaparques</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12 m5">
            <input type="number" name="visitantes" id="visitantes" min="0" value="<?php echo $visitantes ?>">
            <label for="visitantes">Número de Visitantes</label>
          </div>
          <div class="input-field col s12 m7">
            <select name="nacionalidad" id="nacionalidad">
              <option value="" selected><?php echo $nacionalidad ?></option>
              <?php foreach ($visitacionId->CRUDVistaNacionalidadesController() as $key => $v) {
                ?>
                <option value="<?php echo $v['idnacionalidades']; ?>"><?php echo $v['nacion']; ?></option>
                <?php
              } ?>
            </select>
            <label for="nacionalidad">Nacionalidad</label>
          </div>
        </div>

        <div class="row">
          <div class="col s12 m6 l4 lg2">
            <button class="btn waves-effect waves-light" type="submit" name="action">Guardar
              <i class="material-icons right">send</i>
            </button>
          </div>
        </div>
      </form>
    </div>

  </main>

</body>
<?php include "links/foot.php" ?>
<script type="text/javascript">
//Document ready funciona cuando el navegador ha terminado de leer HTML. (Primero)
$(document).ready(function() {
  $('select').material_select();
  $('.datepicker').pickadate({
    format: 'd !de mmmm !del yyyy',
    formatSubmit: 'yyyy/mm/dd',
    hiddenName: true,
    selectMonths: true,
    selectYears: 10,
    monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'jueves', 'Viernes', 'Sabado'],
    weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
    today: 'Hoy',
    clear: 'Borrar',
    close: 'Ok',
    closeOnSelect: false,
    max: new Date()
  });
  //$('select').addClass('browser-default');
  console.log("Document ready.");
});

//Onload funciona cuando toda la aplicación ha termiado de cargarse. (Segundo)
window.onload = function() {
  console.log("Windows onload");
};

//Funciones

</script>
</html>
