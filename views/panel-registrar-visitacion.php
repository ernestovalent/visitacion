<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <?php include "links/head.php" ?>
  <title>Registrar visitación</title>
</head>
<?php
session_start();
if (!$_SESSION["validar"]) {
  header("location:index.php?v=iniciar");
  exit();
}
?>
<?php
$registroVisitacion = new MainController();
$registroVisitacion->CRUDRegistroVisitacionController();
 ?>



<body>
  <style media="screen">
  .pin-foot{
    position: fixed;
    width: 97vw;
    max-width: 1280px;
    bottom: 0px;
    margin: 0px;
    padding-bottom: 0px;
  }
  .card-panel{
    padding: 10px;
  }
  .row{
    margin: 0px;
    padding: 0px;
  }
  .input-field{
    margin: 0px;
  }
  .btn{
    width: 100%;
  }
  @media only screen and (min-width : 601px) {
    .btn{
      width: 60%;
      left: 20%
    }
    .pin-foot{
      width: 93vw;
    }
  }
  @media only screen and (min-width : 993px) {
    .btn{
      width: 50%;
      left: 50%;
    }
    .pin-foot{
      width: 88vw;
    }
  }
  @media only screen and (min-width : 1201px) {
    .btn{
      width: 100%;
      height: 60px;
      left: 0;
      display: flex;
      align-items: center;
    }
  }
</style>
<header>
  <div class="navbar">
    <nav>
      <div class="nav-wrapper container">
        <a href="?v=panel" class="left"><i class="material-icons" style="font-size:3rem;margin-right:2rem;">arrow_back</i></a>
        <a href="#titulo" class="brand-logo">Registrar Visitación</a>
        <ul class="right">
          <li><a href="?panel=editar-visitacion" class="tooltipped" data-tooltip="Editar registros anteriores"><i class="material-icons">&#xE8A0;</i></a></li>
        </ul>
      </div>
    </nav>
  </div>
</header>

<main class="container">
  <form class="" action="" method="post">
    <!-- Encabezado  -->
    <div class="card-panel pushpin-nav">
      <div class="row">
        <div class="input-field col s5 m4 l3">
          <input type="date" name="fecha" id="fecha" class="datepicker" style="margin-bottom:0px;" data-value="<?php echo date("o m d"); ?>" required>
          <label for="fecha">Fecha</label>
        </div>
        <div class="input-field col s7 m4 l3">
          <select name="caseta" id="caseta" required>
            <option value="" disabled selected>Casetas</option>
            <?php
            foreach ($registroVisitacion -> CRUDVistaCasetasController() as $row => $value) {
              ?>
              <option value="<?php echo $value['idcasetas']; ?>"><?php echo $value['estacion']." - ".$value['nombre']; ?></option>
            <?php }
            if (!isset($value['estacion'])) {
              echo '<option value="" disabled>Sin registros</option>';
            } ?>
          </select>
        </div>
        <div class="input-field col s12 m4 l6">
          <select multiple name="guardaparques[]" required>
            <option value="" disabled selected>Guardaparques</option>
            <?php
            foreach ($registroVisitacion -> CRUDVistaGuardaparquesController() as $row => $value) {
              ?>
              <option value="<?php echo $value['idguardaparques']; ?>"><?php echo $value['nombre']." ".$value['apellido']; ?></option>
            <?php }
            if (!isset($value['idguardaparques'])) {
              echo '<option value="" disabled>Sin registros</option>';
            } ?>
          </select>
        </div>
      </div>
    </div>

    <!-- Visitacion -->
    <div class="card-panel" id="divVisitacion" style="margin-bottom:150px;">
      <div class="preloader-wrapper active">
        <div class="spinner-layer spinner-amber">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div><div class="gap-patch">
            <div class="circle"></div>
          </div><div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>
      </div>
    </div>

    <!-- Pie -->
    <div class="card-panel pin-foot z-depth-5" id="divPie" style="z-index:3;">
      <div class="row">
        <div class="col s12 l6 xl5">
          <div class="row">
            <div class="col s12 center">
              Vehículos
            </div>
            <div class="col s3">
              <input type="number" name="bicicletas" id="bicicletas" placeholder="Bicicletas" min="0" style="margin-bottom:0px;">
            </div>
            <div class="col s3">
              <input type="number" name="motos" id="motos" placeholder="Motos" min="0" style="margin-bottom:0px;">
            </div>
            <div class="col s3">
              <input type="number" name="carros" id="carros" placeholder="Carros" min="0" style="margin-bottom:0px;">
            </div>
            <div class="col s3">
              <input type="number" name="carga" id="carga" placeholder="Carga" min="0" style="margin-bottom:0px;">
            </div>
          </div>
        </div>
        <div class="col s12 l6 xl5">
          <div class="row">
            <div class="col s12 center">
              Comentarios
            </div>
            <div class="col s12">
              <textarea name="comentarios" id="comentarios" class="materialize-textarea" style="padding:0px; margin-bottom:0px;" placeholder="Comentarios"></textarea>
            </div>
          </div>
        </div>
        <div class="col s12 xl2">
          <button class="btn waves-effect waves-light" type="submit">Siguiente
            <i class="material-icons right">send</i>
          </button>
        </div>
      </div>
    </div>


  </form>
</main>

</body>
<?php include "links/foot.php" ?>
<script type="text/javascript">
//Document ready funciona cuando el navegador ha terminado de leer HTML. (Primero)
$(document).ready(function() {
  $('select').material_select();
  $('.datepicker').pickadate({
    format: 'd !de mmmm !del yyyy',
    formatSubmit: 'yyyy/mm/dd',
    hiddenName: true,
    selectMonths: true,
    selectYears: 10,
    monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'jueves', 'Viernes', 'Sabado'],
    weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
    today: 'Hoy',
    clear: 'Borrar',
    close: 'Ok',
    closeOnSelect: true,
    max: new Date()
  });

  visitacionSelectAuto();
  $('.tooltipped').tooltip();
  $('.preloader-wrapper').addClass('hide');
  console.log("Document ready.");
});

visitacionInt = 1;
//Funciones
function borrarRow(row){
  if (confirm("Eliminar fila "+row) == true) {
    $('#row'+row).remove();
  }
}
function visitacionSelectAuto(){
  $('#divVisitacion').append('<div class="row" id="row'+visitacionInt+'"></div>');
  $('#divVisitacion #row'+visitacionInt)
  .append('<div class="col s12 m4"><input type="number" name="visitante[]" placeholder="Número" min="0" onfocus="visitacionSelectAuto()" required></div>')
  .append('<div class="col s12 m6"><select class="icons" name="nacion[]" required><option value="" disabled selected>Nacionalidad</option><option value="null" data-icon="" class="circle">Falta nacionalidad</option><?php foreach ($registroVisitacion -> CRUDVistaNacionalidadesController() as $row => $value) { ?><option value="<?php echo $value['idnacionalidades']; ?>" data-icon="" class="circle"><?php echo $value['nacion']; ?></option><?php } ?></select></div>')
  .append('<div class="col s12 m2"><a class="btn-floating btn-flat waves-effect waves-light" onclick="borrarRow('+visitacionInt+')"><i class="material-icons">delete</i></a></div>');
  $('select').material_select();
  visitacionInt = visitacionInt + 1;
  console.log(visitacionInt);
}
function visitacionSelectPlus(){
  $('#divVisitacion').append('<div class="row" id="row'+visitacionInt+'"></div>');
  $('#divVisitacion #row'+visitacionInt)
  .append('<div class="col s12 m4"><input type="number" name="visitante[]" placeholder="Número" min="0" required></div>')
  .append('<div class="col s12 m6"><select class="icons" name="nacion[]" required><option value="" disabled selected>Nacionalidad</option><option value="null" data-icon="" class="circle">Falta nacionalidad</option><?php foreach ($registroVisitacion -> CRUDVistaNacionalidadesController() as $row => $value) { ?><option value="<?php echo $value['idnacionalidades']; ?>" data-icon="" class="circle"><?php echo $value['nacion']; ?></option><?php } ?></select></div>')
  .append('<div class="col s12 m2"><a class="btn-floating btn-flat waves-effect waves-light plus" onclick="visitacionSelectPlus()"><i class="material-icons">add</i></a><a class="btn-floating btn-flat waves-effect waves-light" onfocus="visitacionSelectPlus()" onclick="borrarRow('+visitacionInt+')"><i class="material-icons">delete</i></a></div>');
  $('#divVisitacion #row'+(visitacionInt-1)+' a.plus').addClass('hide')
  //$('#divVisitacion .row')
  $('select').material_select();
  visitacionInt = visitacionInt + 1;
  console.log(visitacionInt);
}
function visitacionAutocomplete(){
  $('#divVisitacion').append('<div class="row" id="row'+visitacionInt+'"><div class="col s12 m4"><input type="number" name="visitantes[]" placeholder="Número" min="0" onfocus="visitacionAutocomplete()" required></div><div class="col s8 m6"><input type="text" name="nacion[]" placeholder="Nacionalidad" class="autocomplete" required></div><div class="col s12 m2"><a class="btn-floating btn-flat waves-effect waves-light" onclick="borrarRow('+visitacionInt+')"><i class="material-icons">delete</i></a></div></div>');
  $('input.autocomplete').autocomplete({
    data: {
      <?php
      foreach ($registroVisitacion -> CRUDVistaNacionalidadesController() as $row => $value) {
        ?>
        "<?php echo $value['nacion']; ?>" : null,
        <?php } ?>
      },
      limit: 5,
      onAutocomplete: function(val) {
        alert("IDNacion");
      },
      minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
    });
    visitacionInt = visitacionInt + 1;
    console.log(visitacionInt);
  }

</script>
</html>
