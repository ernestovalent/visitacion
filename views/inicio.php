<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <?php include "links/head.php" ?>
  <title>Plataforma Visitación</title>
</head>
<body>
  <style media="screen">
    .parallax-container{
      height: 80vh;
    }
  </style>
  <nav>
    <div class="nav-wrapper container">
      <ul class="right">
        <li class="hide-on-med-and-down" style="max-height:54px;">
          <img src="views/img/tec-blanco.png" alt="Logo del Tecnológico" height="64px">
          <img src="views/img/conanp-blanco.png" alt="Logo de la CONANP" height="64px">
          <img src="views/img/semarnat-blanco.png" alt="Logo de SEMARNAT" height="64px">
        </li>
        <li><a href="?v=iniciar" class="waves-effect waves-light btn">INICIAR</a></li>
      </ul>
    </div>
  </nav>
  <main>
    <div class="row container center">
      <div class="col s12">
        <h1 style="font-weight:200">Visitación</h1>
        <p class="flow-text center">Una plataforma donde administrar el acceso y actividades del Complejo Sian Ka'an. <span style="font-size:1rem;">&#8212; Proyecto de Residencia Profesional.</span></p>
      </div>
      <div class="col s12 m6 l3">
        <div class="card-panel">
          <i class="large material-icons">&#xE572;</i>
          <p class="flow-text">Registrar visitación</p>
          <p>Registra información del flujo de visitantes al Complejo Sian Ka’an</p>
        </div>
      </div>
      <div class="col s12 m6 l3">
        <div class="card-panel">
          <i class="large material-icons">&#xE24B;</i>
          <p class="flow-text">Generar Gráficas</p>
          <p>Genere gráficas relevantes con información del registro de Visitación.</p>
        </div>
      </div>
      <div class="col s12 m6 l3">
        <div class="card-panel">
          <i class="large material-icons">&#xE873;</i>
          <p class="flow-text">Generar reportes</p>
          <p>Genera reportes completos en cualquier fecha y con gráficas.</p>
        </div>
      </div>
      <div class="col s12 m6 l3">
        <div class="card-panel" onclick="location.href='?v=manual'" style="cursor: pointer;">
          <i class="large material-icons">&#xE865;</i>
          <p class="flow-text">Documentación</p>
          <p>Consulte todas las opciones y manual de usuario.</p>
        </div>
      </div>
    </div>
    <div class="parallax-container">
      <div class="parallax">
        <img src="views/img/fondo1.png">
      </div>
      <p class="flow-text white-text center" style="padding-top:300px;">Impulsa el Desarrollo Sustentable.</p>
    </div>
    <div class="parallax-container">
      <div class="parallax">
        <img src="views/img/fondo5.jpg">
      </div>
      <p class="flow-text white-text center" style="padding-top:300px;">Una medida de control y seguridad ambiental.</p>
    </div>
    <div class="parallax-container">
      <div class="parallax">
        <img src="views/img/fondo6.jpg">
      </div>
      <p class="flow-text white-text center" style="padding-top:300px;">Estrategia para mejorar la gestión de visitantes.</p>
    </div>
    <div class="parallax-container" style="height:100vh;">
      <div class="parallax">
        <img src="views/img/fondo1.jpg">
      </div>
      <p class="flow-text white-text center" style="padding-top:300px;">Desarrollado por Ernesto Valentin Caamal Peech.</p>
    </div>
  </main>
</body>
<?php include "links/foot.php" ?>
<script>
$(document).ready(function(){
    $('.parallax').parallax();
  });
</script>
</html>
