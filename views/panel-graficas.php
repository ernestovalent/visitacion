<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <?php include "links/head.php" ?>
  <link rel="stylesheet" href="views/css/datatables.css">
  <title>Gráficas generadas</title>
</head>
<?php
session_start();
if (!$_SESSION["validar"]) {
  header("location:index.php?v=iniciar");
  exit();
}
 ?>
<body>
<style media="screen">
canvas{
  background-color: grey;
}
@media only screen and (min-width : 601px) {

}
@media only screen and (min-width : 993px) {

}
@media only screen and (min-width : 1201px) {

}
</style>
<header>
  <div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper container">
        <a href="?panel=grafica" class="left"><i class="material-icons" style="font-size:3rem;margin-right:2rem;">arrow_back</i></a>
        <a href="#titulo" class="brand-logo">Gráficas generadas</a>
        <ul class="right">
          <li><a href="#galeria" class="tooltipped" data-tooltip="Ver todas las gráficas"><i class="material-icons">&#xE3B6;</i></a></li>
        </ul>
      </div>
    </nav>
  </div>
</header>

<main class="container">
  <ul class="collapsible" data-collapsible="accordion">
    <li>
      <div class="collapsible-header active">2017</div>
      <div class="collapsible-body">
        <div class="row">
          <div class="col s12 m6 xl4 card-small">
            <div class="card-image waves-effect waves-block waves-light">
              <img class="activator" src="views/img/grafica-barras.png">
            </div>
          </div>
          <div class="col s12 m6 xl4 card-small">
            <div class="card-image waves-effect waves-block waves-light">
              <img class="activator" src="views/img/grafica-barras.png">
            </div>
          </div>
          <div class="col s12 m6 xl4 card-small">
            <div class="card-image waves-effect waves-block waves-light">
              <img class="activator" src="views/img/grafica-barras.png">
            </div>
          </div>
          <div class="col s12 m6 xl4 card-small">
            <div class="card-image waves-effect waves-block waves-light">
              <img class="activator" src="views/img/grafica-barras.png">
            </div>
          </div>
        </div>
      </div>
    </li>
    <li>
      <div class="collapsible-header">2016</div>
      <div class="collapsible-body">
        <div class="row">
          <div class="col s12 m6 xl4 card-small"></div>
          <div class="col s12 m6 xl4 card-small"></div>
          <div class="col s12 m6 xl4 card-small"></div>
          <div class="col s12 m6 xl4 card-small"></div>
        </div>
      </div>
    </li>
    <li>
      <div class="collapsible-header">2015</div>
      <div class="collapsible-body">
        <div class="row">
          <div class="col s12 m6 xl4 card-small"></div>
          <div class="col s12 m6 xl4 card-small"></div>
          <div class="col s12 m6 xl4 card-small"></div>
          <div class="col s12 m6 xl4 card-small"></div>
        </div>
      </div>
    </li>
  </ul>
</main>

</body>
<?php include "links/foot.php" ?>
<script type="text/javascript">
//Document ready funciona cuando el navegador ha terminado de leer HTML. (Primero)
$(document).ready(function() {
  $('.collapsible').collapsible();

  console.log("Document ready.");
});

//Onload funciona cuando toda la aplicación ha termiado de cargarse. (Segundo)
window.onload = function() {

  console.log("Windows onload");
};

//Funciones generales de javascript.

</script>
</html>
