<?php
if (isset($_POST['html'])) {
  require_once __DIR__ . '/../vendor/autoload.php';
  //$stylesheet = file_get_contents('/views/css/print.css');
  $mpdf = new \Mpdf\Mpdf();
  $mpdf->SetTitle('Reporte de visitacion');
  $mpdf->SetAuthor('CONANP: desarrollador Ernesto Valentin Caamal Peech');
  //$mpdf->WriteHTML($stylesheet,1);
  $mpdf->WriteHTML($_POST['html'],2);
  $mpdf->Output();
}
?>
