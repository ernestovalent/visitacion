function CrearGrafica() {
  console.log("instancia CrearGrafica creada");
}

var Mount = new Array();
Mount[1] = "Enero";
Mount[2] = "Febrero";
Mount[3] = "Marzo";
Mount[4] = "Abril";
Mount[5] = "Mayo";
Mount[6] = "Junio";
Mount[7] = "Julio";
Mount[8] = "Agosto";
Mount[9] = "Septiembre";
Mount[10] = "Octubre";
Mount[11] = "Noviembre";
Mount[12] = "Diciembre";


CrearGrafica.prototype.crearData = function (labels, titulo, data) {
  this.data = {
    labels: labels,
    datasets: [{
      label: titulo,
      backgroundColor: "rgba(255,99,132,0.2)",
      borderColor: "rgba(255,99,132,1)",
      borderWidth: 2,
      hoverBackgroundColor: "rgba(255,99,132,0.4)",
      hoverBorderColor: "rgba(255,99,132,1)",
      data: data,
    }]
  };
  console.log("Data creada");
  //alert(labels.join('\n'));
}

CrearGrafica.prototype.crearDatasVehiculos = function (anios, bicis, motos, autos, carga){
  this.data = {
    labels: anios,
    datasets: [{
      label: "Bicicletas",
      backgroundColor: "rgba(255,99,132,0.2)",
      borderColor: "rgba(255,99,132,1)",
      borderWidth: 2,
      hoverBackgroundColor: "rgba(255,99,132,0.4)",
      hoverBorderColor: "rgba(255,99,132,1)",
      data: bicis,
    },
    {
      label: "Motos",
      backgroundColor: "rgba(255,99,132,0.2)",
      borderColor: "rgba(255,99,132,1)",
      borderWidth: 2,
      hoverBackgroundColor: "rgba(255,99,132,0.4)",
      hoverBorderColor: "rgba(255,99,132,1)",
      data: motos,
    },
    {
      label: "Automoviles",
      backgroundColor: "rgba(255,99,132,0.2)",
      borderColor: "rgba(255,99,132,1)",
      borderWidth: 2,
      hoverBackgroundColor: "rgba(255,99,132,0.4)",
      hoverBorderColor: "rgba(255,99,132,1)",
      data: autos,
    },
    {
      label: "Vehiculos de carga",
      backgroundColor: "rgba(255,99,132,0.2)",
      borderColor: "rgba(255,99,132,1)",
      borderWidth: 2,
      hoverBackgroundColor: "rgba(255,99,132,0.4)",
      hoverBorderColor: "rgba(255,99,132,1)",
      data: carga,
    }]
  };
}

CrearGrafica.prototype.crearOpciones = function () {
  this.options = {
    maintainAspectRatio: true,
    scales: {
      yAxes: [{
        stacked: false,
        gridLines: {
          display: true,
          color: "rgba(255,99,132,0.2)"
        }
      }],
      xAxes: [{
        gridLines: {
          display: true
        },
        ticks: {
          beginAtZero: true
        }
      }]
    }
  };
  console.log("Option creada");
};

CrearGrafica.prototype.crear = function (tipo) {
if (tipo == "bar") {
  Chart.Bar('chart', {
    options: this.options,
    data: this.data
  });
}if (tipo == "lineal") {
  Chart.Line('chart', {
    options: this.options,
    data: this.data
  });
}
  console.log("Crear creada");

};

/*
var data = {
  labels: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul",],
  datasets: [{
    label: "Datos",
    backgroundColor: "rgba(255,99,132,0.2)",
    borderColor: "rgba(255,99,132,1)",
    borderWidth: 2,
    hoverBackgroundColor: "rgba(255,99,132,0.4)",
    hoverBorderColor: "rgba(255,99,132,1)",
    data: [65, 59, 20, 81, 56, 55, 40,],
  }]
};

var options = {
  maintainAspectRatio: false,
  scales: {
    yAxes: [{
      stacked: true,
      gridLines: {
        display: true,
        color: "rgba(255,99,132,0.2)"
      }
    }],
    xAxes: [{
      gridLines: {
        display: false
      }
    }]
  }
};

Chart.Line('chart', {
  options: options,
  data: data
});
*/
