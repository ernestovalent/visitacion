<?php
require_once "../../controllers/controller.php";
require_once "../../models/crud.php";
/**
 *
 */
class Ajax
{
  public $fecha;
  public $caseta;
  public function ajaxVisitacionAnios(){
    $anio = $this->fecha;
    $respuesta = MainController::ajaxVisitacionAniosController($anio);
    echo json_encode($respuesta);
  }
  public function ajaxVisitacionHistorica(){
    $respuesta = MainController::ajaxVisitacionHistoricaController();
    echo json_encode($respuesta);
  }
  public function ajaxVisitacionCasetaAnios(){
    $cas = $this->caseta;
    $respuesta = MainController::ajaxVisitacionCasetaAniosController($cas);
    echo json_encode($respuesta);
  }

  public function ajaxVisitacionCasetaHistorico(){
    $respuesta = MainController::ajaxVisitacionCasetasHistoricoController();
    echo json_encode($respuesta);
  }

  public function ajaxVehiculosCasetaAnio(){
    $cas = $this->caseta;
    $respuesta = MainController::ajaxVehiculosCasetaAnioController($cas);
    echo json_encode($respuesta);
  }

  public function ajaxVehiculosTotales(){
    $respuesta = MainController::ajaxVehiculosTotalesController();
    echo json_encode($respuesta);
  }
}
switch ($_POST['tipo']) {
  case '1':
  $a = new Ajax();
  $a->fecha= $_POST['fecha'];
  $a->ajaxVisitacionAnios();
  break;

  case '2':
  $a = new Ajax();
  $a->ajaxVisitacionHistorica();
  break;

  case '3':
  $a = new Ajax();
  $a->caseta= $_POST['caseta'];
  $a->ajaxVisitacionCasetaAnios();
  break;

  case '4':
  $a = new Ajax();
  $a->ajaxVisitacionCasetaHistorico();
  break;

  case '5':
  $a = new Ajax();
  $a->caseta= $_POST['caseta'];
  $a->ajaxVehiculosCasetaAnio();
  break;

  case '6':
  $a = new Ajax();
  $a->ajaxVehiculosTotales();
  break;

  default:
    alert("Tipo de dato aún no soportado");
    break;
}
