<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <?php include "links/head.php" ?>
  <title>Entrar a Visitación</title>
</head>
<body>
  <nav>
    <div class="nav-wrapper container">
      <a href="?v=inicio" class="brand-logo">Visitación</a>
      <ul class="right hide-on-med-and-down">
        <a>Plataforma para administrar información de visitación del Complejo Sian Ka'an</a>
      </ul>
    </div>
  </nav>
  <main>
    <div class="row section container">
      <div class="col s12 m10 offset-m1 l6 offset-l3">
        <h1 style="font-weight:200;" class="center">Recuperar Inicio</h1>
        <div class="card-panel white center">
          <img src="views/img/conanp.jpg" alt="Logo de la CONANP" height="80px"><br><br>
          <form class="" action="?v=panel" method="post">
            <p class="flow-text">Confirma el correo donde será enviada la contraseña de recuperación.</p>
            <div class="input-field">
              <input type="email" name="usuario" id="usuario" class="validate">
              <label for="usuario" data-error="Escriba un correo válido">Correo:</label>
            </div>
            <button class="btn waves-effect waves-light" type="submit" name="action">Recuperar
              <i class="material-icons right">send</i>
            </button>
          </form>
        </div>
      </div>
    </div>
  </main>
</body>
<?php include "links/foot.php" ?>
</html>
