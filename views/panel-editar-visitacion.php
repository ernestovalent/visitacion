<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <?php include "links/head.php" ?>
  <link rel="stylesheet" href="views/css/datatables.css">
  <title>Editar visitación registrada</title>
</head>
<?php
session_start();
if (!$_SESSION["validar"]) {
  header("location:index.php?v=iniciar");
  exit();
}
$verVisitacion = new MainController();
$fechaInicial = null;
$fechaFinal = null;
if(isset($_POST['fecha-inicial'])){
  $fechaInicial = $_POST['fecha-inicial'];
}
if(isset($_POST['fecha-final'])){
  $fechaFinal = $_POST['fecha-final'];
}

 ?>
<body>
  <style media="screen">

  @media only screen and (min-width : 601px) {

  }
  @media only screen and (min-width : 993px) {

  }
  @media only screen and (min-width : 1201px) {

  }
</style>
<header>
  <div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper container">
        <a href="?panel=registrar-visitacion" class="left"><i class="material-icons" style="font-size:3rem;margin-right:2rem;">arrow_back</i></a>
        <a href="#titulo" class="brand-logo">Editar Visitación</a>
        <!-- Clase que permita imprimir
        <ul class="right">
        <li><a href="#editar" class="tooltipped" data-tooltip="Editar registros anteriores"><i class="material-icons">edit</i></a></li>
      </ul>-->
    </div>
  </nav>
</div>
</header>

<main class="container">
  <!-- Encabezado  -->
  <div class="card-panel">
    <form method="post">
      <div class="row">
        <p>El buscador de registros de visitación permite encontrar resultados entre un rango de fechas, por favor elija una fecha iniciar y una fecha final para buscar.</p>
      </div>
      <div class="row">
        <div class="input-field col s6">
          <input type="date" name="fecha-inicial" id="fecha-inicio" class="datepicker" data-value="<?php echo $fechaInicial; ?>">
          <label for="fecha">Fecha inicial</label>
        </div>
        <div class="input-field col s6">
          <input type="date" name="fecha-final" id="fecha-fin" class="datepicker" data-value="<?php echo $fechaFinal; ?>">
          <label for="fecha">Fecha final</label>
        </div>
        <div class="col s12">
          <button class="btn waves-effect waves-light" type="submit">Ver
            <i class="material-icons right">send</i>
          </button>
        </div>
      </div>
    </form>
  </div>

  <!-- Visitacion -->
  <div class="card-panel" id="divVisitacion">
    <div class="row">
      <div class="col s12">

        <table id="tableVisitacion">
          <thead>
            <tr>
              <th>ID</th>
              <th>Fecha</th>
              <th>Caseta</th>
              <th># Visitantes</th>
              <th>Nacionalidad</th>
              <th>Editar</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if (isset($_POST['fecha-final'])) {
              foreach ($verVisitacion->CRUDVistaVerVisitacionController() as $index => $val) {
                echo "<tr>";
                echo "<td>".$val['idvisitacion']."</td>";
                echo "<td>".$val['fecha']."</td>";
                echo "<td>".$val['caseta']."</td>";
                echo "<td>".$val['visitantes']."</td>";
                echo "<td>".$val['nacionalidad']."</td>";
                echo "<td><a href='?panel=editar-visitacion-id&id=".$val['idvisitacion']."'><i class='small material-icons'>edit</i></a></td>";
                echo "</tr>";
              }
            }
             ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</main>

</body>
<?php include "links/foot.php" ?>
<script type="text/javascript" charset="utf-8" src="node_modules/datatables.net/js/jquery.dataTables.js"></script>
<script type="text/javascript">
//Document ready funciona cuando el navegador ha terminado de leer HTML. (Primero)
$(document).ready(function() {
  $('.datepicker').pickadate({
    format: 'd !de mmmm !del yyyy',
    formatSubmit: 'yyyy/mm/dd',
    hiddenName: true,
    selectMonths: true,
    selectYears: 10,
    monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'jueves', 'Viernes', 'Sabado'],
    weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
    today: 'Hoy',
    clear: 'Borrar',
    close: 'Ok',
    closeOnSelect: true,
    max: new Date()
  });
  $('#tableVisitacion').DataTable({
    language: {
      processing:     "Proceso en curso...",
      search:         "Buscar&nbsp;:",
      lengthMenu:    "Mostrando _MENU_ entradas",
      info:           "Mostrando de _START_ a _END_ entradas de _TOTAL_ totales",
      infoEmpty:      "0 registros procesados.",
      infoFiltered:   "(filtrado de _MAX_ entradas totales)",
      infoPostFix:    "",
      loadingRecords: "Cargando recursos...",
      zeroRecords:    "Ninguna entrada encontrada.",
      emptyTable:     "No existen registros en este rango.",
      paginate: {
        first:      "Primero",
        previous:   "Anterior",
        next:       "Siguiente",
        last:       "Ultimo"
      }
    }
  });
  $('select').material_select();
  console.log("Document ready.");
});

//Onload funciona cuando toda la aplicación ha termiado de cargarse. (Segundo)
window.onload = function() {
  console.log("Windows onload");
};

//Funciones

</script>
</html>
