<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <?php include "links/head.php" ?>
  <title>Configuración de la Plataforma</title>
</head>
<?php
session_start();
if (!$_SESSION["validar"]) {
  header("location:index.php?v=iniciar");
  exit();
}
 ?>
<body>
<style media="screen">
.btn{
  width: 100%;
}
@media only screen and (min-width : 601px) {

}
@media only screen and (min-width : 993px) {

}
@media only screen and (min-width : 1201px) {

}
</style>
<header>
  <div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper container">
        <a href="#atras" class="left" onclick="window.history.back()"><i class="material-icons" style="font-size:3rem;margin-right:2rem;">arrow_back</i></a>
        <a href="#titulo" class="brand-logo">Base de datos</a>
      </div>
    </nav>
  </div>
</header>

<main class="container">
  <div class="card-panel">
    <div class="row">
      <div class="col s12">
        <h4>Crear un respaldo de base de datos</h4>
      </div>
      <div class="col s12 m6 l8">
        <p>Permite guardar un respaldo de base de datos en el servidor que también será descargado.</p>
      </div>
      <div class="col s12 m6 l4">
        <a href="http://localhost/phpmyadmin/db_export.php?db=visitacionbd" target="_blank" class="waves-effect waves-light btn">Crear respaldo</a>
      </div>
    </div>
    <div class="row">
      <div class="col s12">
        <h4>Obtener copia de base de datos por Excel.</h4>
      </div>
      <div class="col s12 m6 l8">
        <p>Permite descargar los datos de Sian Ka'an usados en la Plataforma.
        En formato de Excel .xls</p>
      </div>
      <div class="col s12 m6 l4">
        <a href="http://localhost/phpmyadmin/db_export.php?db=visitacionbd" target="_blank" class="waves-effect waves-light btn">Descargar</a>
      </div>
    </div>
  </div>
</main>

</body>
<?php include "links/foot.php" ?>
<script type="text/javascript">
//Document ready funciona cuando el navegador ha terminado de leer HTML. (Primero)
$(document).ready(function() {
  console.log("Document ready.");
});

//Onload funciona cuando toda la aplicación ha termiado de cargarse. (Segundo)
window.onload = function() {
  console.log("Windows onload");
};
</script>
</html>
