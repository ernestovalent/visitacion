<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <?php include "links/head.php" ?>
  <title>Generar gráficas</title>
</head>
<?php
session_start();
if (!$_SESSION["validar"]) {
  header("location:index.php?v=iniciar");
  exit();
}
?>

<?php
$vistaGrafica = new MainController();
 ?>
<body>
  <style media="screen">
  .row{
    margin: 0px;
  }
  .chart-container {
  position: relative;
  margin: auto;
  height: auto;
  width: 80vw;
  max-width: 1260px;
}
  #mensajeDiv{
    width: 80%;
    min-height: 20px;
    padding: 7px;
    border: 2px dashed #ffcdd2;
    border-radius: 7px;
    background-color: #ffebee;
    color: #ff5252;
  }
  @media only screen and (min-width : 601px) {

  }
  @media only screen and (min-width : 993px) {

  }
  @media only screen and (min-width : 1201px) {

  }
</style>
<header>
  <div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper container">
        <a href="?v=panel" class="left"><i class="material-icons" style="font-size:3rem;margin-right:2rem;">arrow_back</i></a>
        <a href="#titulo" class="brand-logo">Generar gráficas</a>
        <ul class="right">
          <!--<li><a href="?panel=graficas" class="tooltipped" data-tooltip="Ver todas las gráficas"><i class="material-icons">&#xE8A0;</i></a></li>-->
        </ul>
      </div>
    </nav>
  </div>
</header>

<main class="container">
  <div class="card-panel">
    <form method="post">
      <div class="row">
        <div class="col s12"><p class="flow-text">1.- Elige el tipo de datos</p></div>
        <div class="col s12 m6">
          <select name="tipoDatos" id="tipoDatos">
            <option disabled selected></option>
            <optgroup label="Visitación">
              <option value="1">Visitación por cada año</option>
              <option value="2">Visitación historica</option>
              <option value="3">Visitacion de caseta historico</option>
              <option value="4">Visitacion total entre casetas</option>
            </optgroup>
            <optgroup label="Vehiculos">
              <option value="5">Entrada de vehiculos en una caseta</option>
              <option value="6">Tipos de vehiculos historico</option>
            </optgroup>
            <optgroup label="Canje">
              <option value="7" disabled>Canje de brazaletes de un mes</option>
              <option value="8" disabled>Canje de brazaletes del año</option>
              <option value="9" disabled>Canje de brazaletes totales</option>
            </optgroup>
            <optgroup label="Permisionarios">
              <option value="10" disabled>Total permisionarios</option>
            </optgroup>
          </select>
        </div>
      </div>
      <div class="row">
        <div class="col s12"><p class="flow-text">2.- Elige el tipo de gráfica</p></div>
        <div class="col s12 m6">
          <p>
            Gráfica de barras <br>
            <input name="tipoGrafica" type="radio" value="bar" id="graficaBarras" />
            <label for="graficaBarras"><img src="views/img/grafica-barras.png" class="responsive-img" alt="grafica de barras"></label>
          </p>
        </div>
        <div class="col s12 m6">
          <p>
            Gráfica lineal <br>
            <input name="tipoGrafica" type="radio" value="lineal" id="graficaLineal"/>
            <label for="graficaLineal"><img src="views/img/grafica-lineal.png" class="responsive-img" alt="grafica lineal"></label>
          </p>
        </div>
      </div>
      <div class="row hide" id="fechasRow">
        <div class="col s12">
          <p class="flow-text">3.- Elige la fecha</p>
        </div>
        <div class="input-field col s6" id="anioDiv">
          <select name="anio" id="anio">
            <option disabled selected></option>
            <?php
            foreach ($vistaGrafica ->CRUDVistaVisitacionYearController() as $i => $val) {
              echo '<option value="'.$val['anio'].'">'.$val['anio'].'</option>';
            }
             ?>
          </select>
          <label>Año</label>
        </div>
        <div class="input-field col s6 hide" id="mesDiv">
          <select name="mes" id="mes">
            <option disabled selected></option>
            <option value="1">Enero</option>
            <option value="2">Febrero</option>
            <option value="3">Marzo</option>
            <option value="4">Abril</option>
            <option value="5">Mayo</option>
            <option value="6">Junio</option>
            <option value="7">Julio</option>
            <option value="8">Agosto</option>
            <option value="9">Septiembre</option>
            <option value="10">Octubre</option>
            <option value="11">Noviembre</option>
            <option value="12">Diciembre</option>
          </select>
          <label>Mes</label>
        </div>
      </div>
      <div class="row hide" id="casetasRow">
        <div class="col s12">
          <p class="flow-text">4.- Elige la caseta</p>
        </div>
        <div class="input-field col s12">
          <select name="caseta" id="caseta">
            <option disabled selected></option>
            <?php
            foreach ($vistaGrafica->CRUDVistaCasetasController() as $index => $value) {
              echo '<option value="'.$value['idcasetas'].'">'.$value['nombre'].'</option>';
            }
             ?>
          </select>
          <label>Caseta</label>
        </div>
      </div>
      <div class="row center" style="margin-bottom:30px">
        <a class="waves-effect waves-light btn" id="botonGenerar" style="margin-bottom:10px"><i class="material-icons right">insert_chart</i>Generar</a>
        <div class="col s10 offset-s1 center hide" id="mensajeDiv">
          <!-- MENSAJE DE ERROR-->
        </div>
      </div>
      <div class="row hide" id="graficaRow" style="border: 1px dotted black;">
        <div class="chart-container">
          <canvas id="chart"></canvas>
        </div>
        <div class="table-container">

        </div>
      </div>
    </form>
  </div>
</main>

</body>
<?php include "links/foot.php" ?>
<script type="text/javascript" src="node_modules\chart.js\dist\Chart.min.js"></script>
<script type="text/javascript" src="views\js\dataCharts.js"></script>
<script type="text/javascript">
//Document ready funciona cuando el navegador ha terminado de leer HTML. (Primero)
$(document).ready(function() {
  $('select').material_select();
  $('#tipoDatos').change(function(event) {
    $('#graficaRow').addClass('hide');
    switch ($('#tipoDatos').val()) {
      case "1":
        ocultar();
        $('#anio').removeClass('hide');
        $('#fechasRow').removeClass('hide');
        break;
      case "2":
        ocultar();
        break;
      case "3":
        ocultar();
        $('#casetasRow').removeClass('hide');
        break;
      case "4":
        ocultar();
        break;
      case "5":
        ocultar();
        $('#casetasRow').removeClass('hide');
        break;
      case "6":
        ocultar();
        break;
      default:
        ocultar();
    }
  });

  $('#botonGenerar').click(function(event) {
    if (validar()==true) {
      $('#graficaRow').removeClass('hide');
      $('#botonGenerar').addClass('disabled');
      switch ($('#tipoDatos').val()) {
        case "1":
          var anio = $('#anio option:selected').val();
          var datos = new FormData();
          datos.append("fecha", anio);
          datos.append("tipo", "1");
          $.ajax({
            url: 'views/js/ajax.php',
            method: 'POST',
            data: datos,
            cache: false,
            contentType: false,
            processData: false
          })
          .done(function(respuesta) {
            var json = jQuery.parseJSON(respuesta);
            var labelsT = [], dataT=[];
            $.each( json, function( key, value ) {
              labelsT.push(Mount[json[key].mes]);
              dataT.push(parseInt(json[key].total));
            });
            var grafica = new CrearGrafica();
            grafica.crearData(labelsT, "Visitacion del año "+anio, dataT);
            grafica.crearOpciones();
            switch ($('input[name="tipoGrafica"]:checked').val()) {
              case 'bar':
                grafica.crear("bar");
                break;
              case 'lineal':
                grafica.crear("lineal")
              break;
              default:
            }
          })
          .fail(function() {
            alert("Error en Ajax");
          });
          break;

        case "2":
        var datos = new FormData();
        datos.append("tipo", "2");
        $.ajax({
          url: 'views/js/ajax.php',
          method: 'POST',
          data: datos,
          cache: false,
          contentType: false,
          processData: false
        }).done(function(respuesta){
          var json = jQuery.parseJSON(respuesta);
          var labelsT = [], dataT=[];
          $.each( json, function( key, value ) {
            labelsT.push(json[key].anio);
            dataT.push(parseInt(json[key].total));
          });
          var grafica = new CrearGrafica();
          grafica.crearData(labelsT, "Visitacion historica", dataT );
          grafica.crearOpciones();
          switch ($('input[name="tipoGrafica"]:checked').val()) {
            case 'bar':
              grafica.crear("bar");
              break;
            case 'lineal':
              grafica.crear("lineal")
            break;
            default:
          }
        }).fail(function(){
          alert("Error en Ajax");
        });
          break;

        case "3":
        var caseta = $('#caseta option:selected').val();
        var casetaNombre = $('#caseta option:selected').text();
        var datos = new FormData();
        datos.append("caseta", caseta);
        datos.append("tipo", "3");
        $.ajax({
          url: 'views/js/ajax.php',
          method: 'POST',
          data: datos,
          cache: false,
          contentType: false,
          processData: false
        }).done(function(respuesta){
          var json = jQuery.parseJSON(respuesta);
          var labelsT = [], dataT=[];
          $.each( json, function( key, value ) {
            labelsT.push(json[key].anio);
            dataT.push(parseInt(json[key].visitacion));
          });
          var grafica = new CrearGrafica();
          grafica.crearData(labelsT, "Visitacion de la caseta: "+casetaNombre, dataT );
          grafica.crearOpciones();
          switch ($('input[name="tipoGrafica"]:checked').val()) {
            case 'bar':
              grafica.crear("bar");
              break;
            case 'lineal':
              grafica.crear("lineal")
            break;
            default:
          }
        });
          break;

        case "4":
        var datos = new FormData();
        datos.append("tipo", "4");
        $.ajax({
          url: 'views/js/ajax.php',
          method: 'POST',
          data: datos,
          cache: false,
          contentType: false,
          processData: false
        }).done(function(respuesta){
          var json = jQuery.parseJSON(respuesta);
          var labelsT = [], dataT=[];
          $.each( json, function( key, value ) {
            labelsT.push(json[key].caseta);
            dataT.push(parseInt(json[key].visitacion));
          });
          var grafica = new CrearGrafica();
          grafica.crearData(labelsT, "Visitacion total de casetas ", dataT );
          grafica.crearOpciones();
          switch ($('input[name="tipoGrafica"]:checked').val()) {
            case 'bar':
              grafica.crear("bar");
              break;
            case 'lineal':
              grafica.crear("lineal")
            break;
            default:
          }
        });
          break;
        case "5":
        var caseta = $('#caseta option:selected').val();
        var casetaNombre = $('#caseta option:selected').text();
        var datos = new FormData();
        datos.append("caseta", caseta);
        datos.append("tipo", "5");
        $.ajax({
          url: 'views/js/ajax.php',
          method: 'POST',
          data: datos,
          cache: false,
          contentType: false,
          processData: false
        }).done(function(respuesta){
          var json = jQuery.parseJSON(respuesta);
          var labelsT = [], dataBici=[], dataMoto=[], dataAuto=[], dataCarga=[];
          $.each( json, function( key, value ) {
            labelsT.push(json[key].anio);
            dataBici.push(parseInt(json[key].bici));
            dataMoto.push(parseInt(json[key].moto));
            dataAuto.push(parseInt(json[key].auto));
            dataCarga.push(parseInt(json[key].carga));
          });
          var grafica = new CrearGrafica();
          grafica.crearDatasVehiculos(labelsT, dataBici, dataMoto, dataAuto, dataCarga);
          grafica.crearOpciones();
          switch ($('input[name="tipoGrafica"]:checked').val()) {
            case 'bar':
              grafica.crear("bar");
              break;
            case 'lineal':
              grafica.crear("lineal")
            break;
            default:
          }
        });
          break;
        case "6":
        var datos = new FormData();
        datos.append("tipo", "6");
        $.ajax({
          url: 'views/js/ajax.php',
          method: 'POST',
          data: datos,
          cache: false,
          contentType: false,
          processData: false
        }).done(function(respuesta){
          var json = jQuery.parseJSON(respuesta);
          var labelsT = [], dataBici=[], dataMoto=[], dataAuto=[], dataCarga=[];
          $.each( json, function( key, value ) {
            labelsT.push(json[key].caseta);
            dataBici.push(parseInt(json[key].bicis));
            dataMoto.push(parseInt(json[key].motos));
            dataAuto.push(parseInt(json[key].autos));
            dataCarga.push(parseInt(json[key].cargas));
          });
          var grafica = new CrearGrafica();
          grafica.crearDatasVehiculos(labelsT, dataBici, dataMoto, dataAuto, dataCarga);
          grafica.crearOpciones();
          switch ($('input[name="tipoGrafica"]:checked').val()) {
            case 'bar':
              grafica.crear("bar");
              break;
            case 'lineal':
              grafica.crear("lineal")
            break;
            default:
          }
        });
          break;
        default:

      }
    }
  });
//Termina funcion ready de Jquery.
});


function validar(){
  $('#mensajeDiv').removeClass('hide');
  switch ($('#tipoDatos').val()) {
    case "1":
    if ($('input:radio[name=tipoGrafica]').is(':checked')) {
      if ($('#anio option:selected').val()) {
        $('#mensajeDiv').addClass('hide');
        return true;
      }else {
        mensaje("Seleccione el año.");
      }
    }else {
      mensaje("Es necesario elegir un tipo de gráfica.");
    }
      break;
    case "2":
    if ($('input:radio[name=tipoGrafica]').is(':checked')) {
      $('#mensajeDiv').addClass('hide');
      return true;
    }else {
      mensaje("Es necesario elegir un tipo de gráfica.");
    }
      break;
    case "3":
    if ($('input:radio[name=tipoGrafica]').is(':checked')) {
      if ( $('#caseta option:selected').val()) {
        $('#mensajeDiv').addClass('hide');
        return true;
      }else {
        mensaje("Seleccione una caseta.");
      }
    }else {
      mensaje("Es necesario elegir el tipo de gráfica.");
    }
      break;
    case "4":
    if ($('input:radio[name=tipoGrafica]').is(':checked')) {
      $('#mensajeDiv').addClass('hide');
      return true;
    }else {
      mensaje("Es necesario elegir un tipo de gráfica.");
    }
      break;
    case "5":
    if ($('input:radio[name=tipoGrafica]').is(':checked')) {
      if ($('#caseta option:selected').val()) {
        $('#mensajeDiv').addClass('hide');
        return true;
      }else {
        mensaje("Seleccione una caseta.");
      }
    }else {
      mensaje("Es necesario elegir un tipo de gráfica.");
    }
      break;
    case "6":
    if ($('input:radio[name=tipoGrafica]').is(':checked')) {
      $('#mensajeDiv').addClass('hide');
      return true;
    }else {
      mensaje("Es necesario elegir un tipo de gráfica.");
    }
      break;
    default:
    mensaje("Seleccione un tipo de datos aceptable.");
  }
}

function ocultar(){
  $('#mes').addClass('hide');
  $('#anio').addClass('hide');
  $('#fechasRow').addClass('hide');
  $('#casetasRow').addClass('hide');
}

function desocultar(){
  $('#mesDiv').removeClass('hide');
  $('#anioDiv').removeClass('hide');
  $('#fechasRow').removeClass('hide');
  $('#casetasRow').removeClass('hide');
}

function mensaje(text){
  $('#mensajeDiv').removeClass('hide').text(text);
}
</script>
</html>
