<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <?php include "links/head.php" ?>
  <title>Configuración de la Plataforma</title>
</head>
<?php
session_start();
if (!$_SESSION["validar"]) {
  header("location:index.php?v=iniciar");
  exit();
}
$vistaSianKaan = new MainController();
$vistaSianKaan->CRUDRegistroCasetasController();
$vistaSianKaan->CRUDRegistroGuardaparquesController();
$vistaSianKaan->CRUDActualizarCasetasController();
$vistaSianKaan->CRUDActualizarGuardaparqueController();
$vistaSianKaan->CRUDBorrarCasetaController();
$vistaSianKaan->CRUDBorrarGuardaparqueController();
?>
<body>
  <style media="screen">
  .btn{
    width: 100%;
  }
  .erase:hover{
    background-color: #e57373 ;
    color: white;
  }
  @media only screen and (min-width : 601px) {

  }
  @media only screen and (min-width : 993px) {

  }
  @media only screen and (min-width : 1201px) {

  }
</style>
<header>
  <div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper container">
        <a href="?panel=configuracion" class="left"><i class="material-icons" style="font-size:3rem;margin-right:2rem;">arrow_back</i></a>
        <a href="#titulo" class="brand-logo">Configuración</a>
      </div>
    </nav>
  </div>
</header>

<main class="container">
  <div class="card-panel">
    <div class="row">
      <div class="col s12"><h5>Casetas</h5></div>
      <form method="post" id="formVistaCasetas">
        <?php
        foreach ($vistaSianKaan -> CRUDVistaCasetasController() as $row => $value) {
          ?>
          <div class="row">
            <div class="input-field col s4">
              <input disabled type="text" name="estacion" id="<?php echo "estacion".$value['idcasetas']; ?>" value="<?php echo $value['estacion']; ?>">
              <label for="estacion">Estación</label>
            </div>
            <div class="input-field col s8 m6">
              <input disabled type="text" name="caseta" id="<?php echo "caseta".$value['idcasetas']; ?>" value="<?php echo $value['nombre']; ?>">
              <label for="caseta">Caseta</label>
            </div>
            <div class="input-field">
              <input disabled type="text" class="hide" name="idcaseta" id="<?php echo "idcaseta".$value['idcasetas']; ?>" value="<?php echo $value['idcasetas']; ?>">
            </div>
            <div class="col s12 m2">
              <a class="btn-floating btn-flat waves-effect waves-light" id="<?php echo 'botonCasetaEditar'.$value['idcasetas']; ?>" onclick="editarCasetas('<?php echo $value['idcasetas']; ?>')"><i class="material-icons">edit</i></a>
              <a class="btn-floating btn-flat waves-effect waves-light hide" id="<?php echo 'botonCasetaGuardar'.$value['idcasetas']; ?>" onclick="submitCaseta()"><i class="material-icons">save</i></a>
              <a class="btn-floating btn-flat waves-effect waves-light erase" id="<?php echo 'botonCasetaEliminar'.$value['idcasetas'];?>" onclick="borrarCaseta(<?php echo $value['idcasetas'].",'".$value['nombre']; ?>')"><i class="material-icons">delete</i></a>
            </div>
          </div>
        <?php }
        if (!isset($value['idcasetas'])) {
          echo 'No existe ningún registro. Pruebe <a class="botonVolveraRegistrar" href="#">agregar uno nuevo.</a>';
        }  ?>
      </form>
    </div>
    <div class="row">
      <form method="post" id="formVistaGuardaparques">
        <div class="col s12"><h5>Guardaparques</h5></div>
        <?php
        foreach ($vistaSianKaan -> CRUDVistaGuardaparquesController() as $row => $value) {
          ?>
          <div class="row">
            <div class="input-field col s12 l2 hide-on-med-and-down">
              <input disabled name="clave" id="<?php echo "clave".$value['idguardaparques']; ?>" type="text" value="<?php echo $value['clave']; ?>">
              <label for="clave">Clave</label>
            </div>
            <div class="input-field col s12 m5 l4">
              <input disabled name="guardaparqueNombre" id="<?php echo "guardaparqueNombre".$value['idguardaparques']; ?>" type="text" value="<?php echo $value['nombre']; ?>">
              <label for="guardaparqueNombre">Nombre</label>
            </div>
            <div class="input-field col s12 m5 l4">
              <input disabled name="guardaparqueApellido" id="<?php echo "guardaparqueApellido".$value['idguardaparques']; ?>" type="text" value="<?php echo $value['apellido']; ?>">
              <label for="guardaparqueApellido">Apellido</label>
            </div>
            <div class="input-field">
              <input disabled type="text" class="hide" name="idguardaparque" id="<?php echo "idguardaparque".$value['idguardaparques']; ?>" value="<?php echo $value['idguardaparques']; ?>">
            </div>
            <div class="col s12 m2 l2">
              <a class="btn-floating btn-flat waves-effect waves-light" id="<?php echo 'botonGuardaparqueEditar'.$value['idguardaparques']; ?>" onclick="editarGuardaparque('<?php echo $value['idguardaparques']; ?>')"><i class="material-icons">edit</i></a>
              <a class="btn-floating btn-flat waves-effect waves-light hide" id="<?php echo 'botonGuardaparqueGuardar'.$value['idguardaparques']; ?>" onclick="submitGuardaparque()"><i class="material-icons">save</i></a>
              <a class="btn-floating btn-flat waves-effect waves-light erase" id="<?php echo 'botonGuardaparqueEliminar'.$value['idguardaparques'];?>" onclick="borrarGuardaparque(<?php echo $value['idguardaparques'].",'".$value['nombre']; ?>')"><i class="material-icons">delete</i></a>
            </div>
          </div>
        <?php }
        if (!isset($value['idguardaparques'])) {
          echo 'No existe ningún registro. Pruebe <a class="botonVolveraRegistrar" href="#">agregar uno nuevo.</a>';
        }  ?>
      </form>
    </div>
  </div>
</main>

<div id="modalAgregarCaseta" class="modal">
  <form id="formRegistroCasetas" method="post">
    <div class="modal-content">
      <h4>Agregar caseta</h4>
      <p>Agrega una caseta a la base de datos de Sian Ka'an.</p>
      <div class="row">
        <div class="input-field col s12 m4">
          <input type="text" name="nuevaCasetaEstacion" class="validate"  data-length="5">
          <label for="nuevaCasetaEstacion">Estación (Clave)</label>
        </div>
        <div class="input-field col s12 m8">
          <input type="text" name="nuevaCasetaNombre" class="validate" data-length="45" required>
          <label for="nuevaCasetaNombre">Nombre de la caseta</label>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button class="modal-action modal-close btn-flat waves-effect waves-light" type="submit">Guardar
        <i class="material-icons right">send</i>
      </button>
    </div>
  </form>
</div>
<div id="modalAgregarGuardaparque" class="modal">
  <form id="formRegistroGuardaparque" method="post">
    <div class="modal-content">
      <h4>Agregar guardaparque</h4>
      <p>Agregar un guardaparque a la base de datos de Sian Ka'an.</p>
      <div class="row">
        <div class="input-field col s12 m6">
          <input type="text" name="nuevoGuardaparqueNombre" class="validate"  data-length="45" required>
          <label for="nuevoGuardaparqueNombre">Nombres</label>
        </div>
        <div class="input-field col s12 m6">
          <input type="text" name="nuevoGuardaparqueApellido" class="validate"  data-length="45" required>
          <label for="nuevoGuardaparqueApellido">Apeliidos</label>
        </div>
        <div class="input-field col s6">
          <input type="text" name="nuevoGuardaparqueClave" class="validate"  data-length="15">
          <label for="nuevoGuardaparqueApellido">Clave interna</label>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button class="modal-action modal-close btn-flat waves-effect waves-light" type="submit">Guardar
        <i class="material-icons right">send</i>
      </button>
      <!--<a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Agregar</a>-->
    </div>
  </form>
</div>
<div id="modalEliminar" class="modal">
  <div class="modal-content">
    <h4>¿Realmente desea eliminar?</h4>
    <p id="modalEliminarTexto"></p>
    <p>Al borrar estos datos no volverán a aparecer en el sistema, sin embargo seguirán apareciendo en los registros guardados.
      Para agregar nuevamente deberá <a class="botonVolveraRegistrar">volver a registrarlo.</a></p>
    </div>
    <div class="modal-footer">
      <a class="modal-action modal-close waves-effect waves-green btn-flat">Cancelar</a>
      <a id="modalEliminarSubmit" class="modal-action modal-close waves-effect waves-green btn-flat erase">Borrar</a>
    </div>
  </div>

  <div class="fixed-action-btn">
    <a id="botonAgregar" class="btn-floating btn-large">
      <i class="large material-icons">add</i>
    </a>
    <ul>
      <li><a class="btn-floating tooltipped modal-trigger" href="#modalAgregarCaseta" data-tooltip="Agregar caseta" data-position="left" data-delay="10"><i class="material-icons">home</i></a></li>
      <li><a class="btn-floating tooltipped modal-trigger" href="#modalAgregarGuardaparque" data-tooltip="Agregar guardaparque" data-position="left" data-delay="10"><i class="material-icons">person_add</i></a></li>
    </ul>
  </div>
  <div class="tap-target" data-activates="botonAgregar">
    <div class="tap-target-content">
      <h5>Botón registrar</h5>
      <p>Este botón permite registrar una nueva caseta o un nuevo guardaparque.</p>
    </div>
  </div>

</body>
<?php include "links/foot.php" ?>

<script type="text/javascript">
//Document ready funciona cuando el navegador ha terminado de leer HTML. (Primero)
$(document).ready(function() {
  $('.modal').modal();
  console.log("Document ready.");
});

//Onload funciona cuando toda la aplicación ha termiado de cargarse. (Segundo)
window.onload = function() {
  $('.botonVolveraRegistrar').click(function(event) {
    $('#modalEliminar').modal('close');
    $('.tap-target').tapTarget('open');
  });
  $('a[href="#modalAgregarCaseta"]').click(function(event){
    $('#modalAgregarCaseta').modal('open');
    $('input[name="nuevaCasetaEstacion"]').focus();
  });
  $('a[href="#modalAgregarGuardaparque"]').click(function(event){
    $('#modalAgregarGuardaparque').modal('open');
    $('input[name="nuevoGuardaparqueNombre"]').focus();
  });
  console.log("Windows onload");
};

function editarCasetas(id){
  $('form#formVistaCasetas .input-field > input#estacion'+id).removeAttr('disabled');
  $('form#formVistaCasetas .input-field > input#caseta'+id).removeAttr('disabled');
  $('form#formVistaCasetas .input-field > input#idcaseta'+id).removeAttr('disabled');
  $('a#botonCasetaEditar'+id).addClass('hide');
  $('a#botonCasetaGuardar'+id).removeClass('hide');
}

function editarGuardaparque(id){
  $('form#formVistaGuardaparques .input-field > input#clave'+id).removeAttr('disabled');
  $('form#formVistaGuardaparques .input-field > input#guardaparqueNombre'+id).removeAttr('disabled');
  $('form#formVistaGuardaparques .input-field > input#guardaparqueApellido'+id).removeAttr('disabled');
  $('form#formVistaGuardaparques .input-field > input#idguardaparque'+id).removeAttr('disabled');
  $('a#botonGuardaparqueEditar'+id).addClass('hide');
  $('a#botonGuardaparqueGuardar'+id).removeClass('hide');
}

function submitCaseta(){
  $('form#formVistaCasetas').submit();
}

function submitGuardaparque(){
  $('form#formVistaGuardaparques').submit();
}

function borrarCaseta(id, nombre){
  $('form#formVistaCasetas').append('<input type="text" name="borrarCaseta" value="true" class="hide">');
  $('form#formVistaCasetas .input-field > input#idcaseta'+id).removeAttr('disabled');
  $('#modalEliminarTexto').text('La caseta "'+nombre+'" con el número ID: '+id+' en base de datos.');
  $('#modalEliminar').modal('open');
  $('#modalEliminarSubmit').click(function(event) {
    submitCaseta();
  });
}

function borrarGuardaparque(id, nombre){
  $('form#formVistaGuardaparques').append('<input type="text" name="borrarGuardaparque" value="true" class="hide">');
  $('form#formVistaGuardaparques .input-field > input#idguardaparque'+id).removeAttr('disabled');
  $('#modalEliminarTexto').text('Al guardaparque "'+nombre+'" con el número ID: '+id+' en base de datos.');
  $('#modalEliminar').modal('open');
  $('#modalEliminarSubmit').click(function(event) {
    submitGuardaparque();
  });
}


</script>
</html>
