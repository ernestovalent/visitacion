<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <?php include "links/head.php" ?>
  <link rel="stylesheet" href="views/css/datatables.css">
  <title>Generar gráficas</title>
</head>
<?php
session_start();
if (!$_SESSION["validar"]) {
  header("location:index.php?v=iniciar");
  exit();
}
 ?>
 <?php
$vistaReporte = new MainController();
  ?>
<body>
<style media="screen">
#mensajeDiv{
  width: 80%;
  min-height: 20px;
  padding: 7px;
  border: 2px dashed #ffecb3;
  border-radius: 7px;
  background-color: #fff8e1;
  color: #212121;
}
@media only screen and (min-width : 601px) {

}
@media only screen and (min-width : 993px) {

}
@media only screen and (min-width : 1201px) {

}
</style>
<header>
  <div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper container">
        <a href="?v=panel" class="left"><i class="material-icons" style="font-size:3rem;margin-right:2rem;">arrow_back</i></a>
        <a href="#titulo" class="brand-logo">Generar Reporte</a>
        <!--
        Falta agregar vista de reportes generados
        <ul class="right">
          <li><a href="?panel=reporte-generado" class="tooltipped" data-tooltip="Ver todas los reportes"><i class="material-icons">&#xE8A0;</i></a></li>
        </ul>-->
      </div>
    </nav>
  </div>
</header>

<main class="container">
  <div class="card-panel">
    <form method="get" action="?panel=reporte-generado">
      <input type="hidden" name="panel" value="reporte-generado">
      <div class="row">
        <div class="col s12"><p class="flow-text">1.- Elige el tipo de datos</p></div>
        <div class="col s6">
          <select name="d" id="tipoDatos">
            <option disabled ></option>
            <option value="1" selected>Visitacion</option>
            <option value="2" disabled>Canje de brazaletes</option>
            <option value="3" disabled>Permisionarios</option>
          </select>
        </div>
      </div>
      <div class="row" id="fechasRow">
        <div class="col s12">
          <p class="flow-text">3.- Elige las fechas del reporte</p>
        </div>
        <div class="input-field col s6" id="anioDiv">
          <select name="a" id="anio" required>
            <option disabled selected></option>
            <?php
            foreach ($vistaReporte -> CRUDVistaVisitacionYearController()  as $key => $value) {
              echo '<option value="'.$value['anio'].'">'.$value['anio'].'</option>';
            }
             ?>
          </select>
        </div>
      </div>
      <div class="row center" style="margin-bottom:30px">
        <button class="btn waves-effect waves-light" type="submit">Generar reporte
          <i class="material-icons right">description</i>
        </button>
        <div class="col s10 offset-s1 center" id="mensajeDiv" style="margin-top:10px;">
          El reporte se genera usando los datos y graficas registrados, hasta la fecha. <br>
          Si un reporte ha sido generado anteriormente, será reemplazado.
        </div>
      </div>
    </form>
  </div>
</main>

</body>
<?php include "links/foot.php" ?>

<script type="text/javascript">
//Document ready funciona cuando el navegador ha terminado de leer HTML. (Primero)
$(document).ready(function() {
  $('select').material_select();

  console.log("Document ready.");
});

//Onload funciona cuando toda la aplicación ha termiado de cargarse. (Segundo)
window.onload = function() {

  console.log("Windows onload");
};

//Funciones

</script>
</html>
