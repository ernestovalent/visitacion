<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <?php include "links/head.php" ?>
  <title>Configuración de la Plataforma</title>
</head>
<?php
session_start();
if (!$_SESSION["validar"]) {
  header("location:index.php?v=iniciar");
  exit();
}
$vistaCuenta = new MainController();
$vistaCuenta -> CRUDActualizarUsuariosController();
$administrador = null;
$direccion =  null;
$subdireccion = null;
 ?>
<body>
<style media="screen">
.btn{
  width: 100%;
}
@media only screen and (min-width : 601px) {

}
@media only screen and (min-width : 993px) {

}
@media only screen and (min-width : 1201px) {

}
</style>
<header>
  <div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper container">
        <a href="?panel=configuracion" class="left"><i class="material-icons" style="font-size:3rem;margin-right:2rem;">arrow_back</i></a>
        <a href="#titulo" class="brand-logo">Configuración</a>
      </div>
    </nav>
  </div>
</header>

<main class="container">
  <div class="card-panel">
    <div class="row">
      <form method="post" id="formCuentaAdministrador">
        <?php
        foreach ($vistaCuenta -> CRUDVistaUsuariosController("2") as $row => $value) {
         ?>
        <div class="col s12"><h5>Cuenta de administrador</h5></div>
        <div class="input-field col s12 m6">
          <input disabled id="adminNombre" name="adminNombre" type="text" class="validate" value="<?php echo $value['nombre']; ?>">
          <label for="adminNombre">Nombres</label>
        </div>
        <div class="input-field col s12 m6">
          <input disabled id="adminApellido" name="adminApellido" type="text" class="validate" value="<?php echo $value['apeliido']; ?>">
          <label for="adminApellido">Apellidos</label>
        </div>
        <div class="input-field col s12 m6">
          <input disabled id="adminCorreoUno" name="adminCorreoUno" type="email" class="validate" value="<?php echo $value['correo']; ?>">
          <label for="adminCorreoUno" data-error="Verifica el formato de correo, asegure que sea institucional" data-success="right">Correo Institucional</label>
        </div>
        <div class="input-field col s12 m6 hide">
          <input id="adminCorreoDos" name="adminCorreoDos" type="email" class="validate">
          <label for="adminCorreoDos" data-error="Verifica el formato de correo." data-success="right">Correo de recuperación</label>
        </div>
        <div class="input-field col s12 m6">
          <input disabled id="adminContraUno" name="adminContraUno" type="password" class="validate" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" value="<?php echo $value['contra']; ?>">
          <label for="adminContraUno" data-error="Su contraseña debe tener al menos una mayúscula, una minúscula y un número">Contraseña</label>
        </div>
        <div class="input-field col s12 m6 hide">
          <input id="adminContraDos" name="adminContraDos" type="password" class="validate" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}">
          <label for="adminContraDos" data-error="Su contraseña debe coincidir con la anterior, tener al menos una mayúscula, una minúscula y un número">Confirmar contraseña</label>
        </div>
        <div class="col s12 m6 l4 lg2">
          <a class="waves-effect waves-light btn" onclick="editar('formCuentaAdministrador')"><i class="material-icons right">edit</i>Modificar</a>
          <button class="btn waves-effect waves-light hide" type="submit" name="action" style="margin-top:20px;">Guardar
            <i class="material-icons right">send</i>
          </button>
        </div>
      <?php
    $administrador = "admin";
     }
      if ($administrador == null) {
        echo "No existe el usuario <b>Administrador</b>, realice el registro en base de datos.";
      }
      ?>
      </form>
    </div>
    <div class="row">
      <form method="post" id="formCuentaSubdireccion">
        <?php
        foreach ($vistaCuenta -> CRUDVistaUsuariosController("4") as $row => $value) {
        ?>
        <div class="col s12"><h5>Subdirección</h5></div>
        <div class="input-field col s12 m6">
          <input disabled id="subNombre" name="subNombre" type="text" class="validate" value="<?php echo $value['nombre']; ?>">
          <label for="subNombre">Nombre</label>
        </div>
        <div class="input-field col s12 m6">
          <input disabled id="subApellido" name="subApellido" type="text" class="validate" value="<?php echo $value['apeliido']; ?>">
          <label for="subApellido">Apellido</label>
        </div>
        <div class="input-field col s12 m6">
          <input disabled id="SubCorreo" name="SubCorreo" type="email" class="validate" value="<?php echo $value['correo']; ?>">
          <label for="SubCorreo" data-error="Verifica el formato de correo, asegure que sea institucional" data-success="right">Correo Institucional</label>
        </div>
        <div class="input-field col s12 m6">
          <input disabled id="subContra" name="subContra" type="password" class="validate" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" value="<?php echo $value['contra']; ?>">
          <label for="subContra" data-error="Su contraseña debe tener al menos una mayúscula, una minúscula y un número">Contraseña</label>
        </div>
        <div class="col s12 m6 l4 lg2">
          <a class="waves-effect waves-light btn" onclick="editar('formCuentaSubdireccion')"><i class="material-icons right">edit</i>Modificar</a>
          <button class="btn waves-effect waves-light hide" type="submit" name="action" style="margin-top:20px;">Guardar
            <i class="material-icons right">send</i>
          </button>
        </div>
      <?php
    $direccion = "null";
     }
      if ($direccion == null) {
        echo "No existe el usuario <b>Subdirección</b>, realice el registro en base de datos.";
      } ?>
      </form>
    </div>
    <div class="row">
      <form method="post" id="formCuentaDireccion">
        <?php
        foreach ($vistaCuenta -> CRUDVistaUsuariosController("3") as $row => $value) {
          ?>
        <div class="col s12"><h5>Dirección</h5></div>
        <div class="input-field col s12 m6">
          <input disabled id="dirNombre" name="dirNombre" type="text" class="validate" value="<?php echo $value['nombre']; ?>">
          <label for="dirNombre">Nombre</label>
        </div>
        <div class="input-field col s12 m6">
          <input disabled id="dirApellido" name="dirApellido" type="text" class="validate" value="<?php echo $value['apeliido']; ?>">
          <label for="dirApellido">Apellido</label>
        </div>
        <div class="input-field col s12 m6">
          <input disabled id="dirCorreo" name="dirCorreo" type="email" class="validate" value="<?php echo $value['correo']; ?>">
          <label for="dirCorreo" data-error="Verifica el formato de correo, asegure que sea institucional">Correo Institucional</label>
        </div>
        <div class="input-field col s12 m6">
          <input disabled id="dirContra" name="dirContra" type="password" class="validate" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" value="<?php echo $value['contra']; ?>">
          <label for="dirContra" data-error="Su contraseña debe tener al menos una mayúscula, una minúscula y un número">Contraseña</label>
        </div>
        <div class="col s12 m6 l4 lg2">
          <a class="waves-effect waves-light btn" onclick="editar('formCuentaDireccion')"><i class="material-icons right">edit</i>Modificar</a>
          <button class="btn waves-effect waves-light hide" type="submit" name="action" style="margin-top:15px;">Guardar
            <i class="material-icons right">send</i>
          </button>
        </div>
      <?php
    $subdireccion = "null";
    }
      if ($subdireccion == null) {
        echo "No existe el usuario <b>Dirección</b>, realice el registro en base de datos.";
      } ?>
      </form>
    </div>
  </div>
</main>

</body>
<?php include "links/foot.php" ?>
<script type="text/javascript">
//Document ready funciona cuando el navegador ha terminado de leer HTML. (Primero)
$(document).ready(function() {
  console.log("Document ready.");
});

//Onload funciona cuando toda la aplicación ha termiado de cargarse. (Segundo)
function editar(form){
  $("form#"+form+" .input-field > input").removeAttr('disabled');
  $("form#"+form+" .input-field").removeClass('hide');
  $("form#"+form+" div > button").removeClass('hide');
  $("form#"+form+" div > a").addClass('hide');
}
window.onload = function() {
  console.log("Windows onload");
};
</script>
</html>
