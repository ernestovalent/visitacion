<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <?php include "links/head.php" ?>
  <title>Entrar a Visitación</title>
</head>
<?php
session_start();
if (isset($_SESSION["validar"])) {
  if ($_SESSION["validar"]) {
    header("location:index.php?v=panel");
    exit();
  }
}
 ?>
<body>
  <nav>
    <div class="nav-wrapper container">
      <a href="?v=inicio" class="brand-logo">Plataforma Visitación</a>
      <ul class="right hide-on-med-and-down">
        <a href="?v=manual">Plataforma para administrar información de visitación del Complejo Sian Ka'an</a>
      </ul>
    </div>
  </nav>
  <main>
    <div class="row container">
      <div class="col s12 m10 offset-m1 l6 offset-l3">
        <h1 style="font-weight:200;" class="center">Iniciar Sesión</h1>
        <div class="card-panel white center">
          <img src="views/img/conanp.jpg" alt="Logo de la CONANP" height="80px"><br><br>
          <form method="post">
            <div class="input-field">
              <input type="email" name="usuario" id="usuario" class="validate" pattern=".+@conanp.gob.mx" autofocus>
              <label for="usu" data-error="Escriba su correo institucional.">Correo:</label>
            </div>
            <div class="input-field">
              <input type="password" name="password" id="password" class="validate" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}">
              <label for="password" data-error="La contraseña debe tener 1 mayúscula, 1 minúscula y 1 número al menos.">Contraseña:</label>
            </div>
            <div class="left-align" style="margin-top:20px;">
              <a href="?v=recuperar">Recuperar acceso.</a>
            </div>
            <button class="btn waves-effect waves-light" type="submit" name="action">Iniciar
              <i class="material-icons right">send</i>
            </button>
          </form>
          <?php
            $ingreso =  new MainController();
            $ingreso -> CRUDIngresoPlataformaController();
          ?>
        </div>
      </div>
    </div>
  </main>
</body>
<?php include "links/foot.php" ?>
</html>
