<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <?php include "links/head.php" ?>
  <title>Configuración de la Plataforma</title>
</head>
<?php
session_start();
if (!$_SESSION["validar"]) {
  header("location:index.php?v=iniciar");
  exit();
}
 ?>
<body>
<style media="screen">
.btn{
  width: 100%;
}
@media only screen and (min-width : 601px) {

}
@media only screen and (min-width : 993px) {

}
@media only screen and (min-width : 1201px) {

}
</style>
<header>
  <div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper container">
        <a href="?v=panel" class="left"><i class="material-icons" style="font-size:3rem;margin-right:2rem;">arrow_back</i></a>
        <a href="#titulo" class="brand-logo">Configuración</a>
      </div>
    </nav>
  </div>
</header>

<main class="container">
  <div class="card-panel">
    <div class="row">
      <div class="col s12">
        <h4>Administrar cuenta</h4>
      </div>
      <div class="col s12 m6 l8">
        <p>Permite administrar información de las cuenta, su nombre,
          el correo de recuperación y su contraseña. Asi como administrar información de la cuenta
        de dirección y subdirección. </p>
      </div>
      <div class="col s12 m6 l4">
        <a href="?configurar=cuenta" class="waves-effect waves-light btn">Cuentas</a>
      </div>
    </div>
    <div class="row">
      <div class="col s12">
        <h4>Administrar datos de Sian Ka'an</h4>
      </div>
      <div class="col s12 m6 l8">
        <p>Permite administrar los datos de Sian Ka'an usados en la Plataforma.
        Como por ejemplo, información de los guardaparques, casetas o editar nacionalidad</p>
      </div>
      <div class="col s12 m6 l4">
        <a href="?configurar=siankaan" class="waves-effect waves-light btn">Sian Ka'an</a>
      </div>
    </div>
    <div class="row">
      <div class="col s12">
        <h4>Administrar base de datos de visitación.</h4>
      </div>
      <div class="col s12 m6 l8">
        <p>Permite administrar la base de datos de "Visitación".</p>
      </div>
      <div class="col s12 m6 l4">
        <a href="?configurar=basededatos" class="waves-effect waves-light btn">Base de datos</a>
      </div>
    </div>
    <div class="row">
      <div class="col s12">
        <h4>Consultar el Manual de la Plataforma.</h4>
      </div>
      <div class="col s12 m6 l8">
      </div>
      <div class="col s12 m6 l4">
        <a href="?v=manual" class="waves-effect waves-light btn">Manual de la Plataforma</a>
      </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
  </div>
</main>

</body>
<?php include "links/foot.php" ?>
<script type="text/javascript">
//Document ready funciona cuando el navegador ha terminado de leer HTML. (Primero)
$(document).ready(function() {
  console.log("Document ready.");
});

//Onload funciona cuando toda la aplicación ha termiado de cargarse. (Segundo)
window.onload = function() {
  console.log("Windows onload");
};
</script>
</html>
