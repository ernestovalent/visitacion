<?php
class Navegacion{
  //https://xd.adobe.com/view/00f8de87-87bf-4d64-9055-0fc300e3fb68/

  public function EnlacesPaginasModelos($enlacesModelo){
    if ($enlacesModelo == "inicio" ||
        $enlacesModelo == "iniciar" ||
        $enlacesModelo == "panel"||
        $enlacesModelo == "recuperar" ||
        $enlacesModelo == "test"||
        $enlacesModelo == "manual"||
        $enlacesModelo == "documentacion") {

          $modulo = "views/".$enlacesModelo.".php";

    }elseif ($enlacesModelo == "registrar-visitacion" ||
            $enlacesModelo == "editar-visitacion" ||
            $enlacesModelo == "editar-visitacion-id" ||
            $enlacesModelo == "grafica"||
            $enlacesModelo == "graficas"||
            $enlacesModelo == "reporte"||
            $enlacesModelo == "reportes"||
            $enlacesModelo == "reporte-generado"||
            $enlacesModelo == "configuracion" ||
            $enlacesModelo == "pdf") {

      $modulo = "views/panel-".$enlacesModelo.".php";

    }elseif ($enlacesModelo == "cuenta"||
              $enlacesModelo == "siankaan"||
              $enlacesModelo == "basededatos") {

      $modulo = "views/configurar-".$enlacesModelo.".php";

    }elseif ($enlacesModelo == "salir") {
      $modulo = "views/salir.php";
    } else {

      $modulo = "views/inicio.php";

    }

    return $modulo;

  }

}
?>
