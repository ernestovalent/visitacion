SELECT
  `visitacion`.`idvisitacion` AS idvisitacion,
  `visitacion`.`fecha` AS fecha,
  `casetas`.`idcasetas` AS idcasetas,
  `casetas`.`nombre` AS caseta,
  `visitacion_vehiculos`.`idvehiculos` AS idvehiculos,
  `visitacion_vehiculos`.`num_bici` AS bici,
  `visitacion_vehiculos`.`num_moto` AS moto,
  `visitacion_vehiculos`.`num_auto` AS auto,
  `visitacion_vehiculos`.`num_carga` AS carga
FROM
  `visitacion`
LEFT JOIN
	`casetas`
    ON
    `casetas`.`idcasetas` = `visitacion`.`idcasetas`
LEFT JOIN
  `visitacion_vehiculos`
    ON
    `visitacion`.`idvisitacion` = `visitacion_vehiculos`.`idvisitacion`
 ORDER BY `visitacion`.`idvisitacion` ASC
