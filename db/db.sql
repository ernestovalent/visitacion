-- MySQL Script generated by MySQL Workbench
-- Thu Oct 26 11:08:25 2017
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema visitacionbd
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema visitacionbd
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `visitacionbd` DEFAULT CHARACTER SET utf8 ;
USE `visitacionbd` ;

-- -----------------------------------------------------
-- Table `visitacionbd`.`usuarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitacionbd`.`usuarios` (
  `idusuarios` TINYINT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(25) NOT NULL,
  `apeliido` VARCHAR(25) NULL,
  `correo` VARCHAR(40) NOT NULL,
  `contra` VARCHAR(45) BINARY NULL DEFAULT NULL,
  `activo` TINYINT NOT NULL DEFAULT 1,
  `fecha_creacion` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `actualizacion` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idusuarios`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `visitacionbd`.`casetas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitacionbd`.`casetas` (
  `idcasetas` TINYINT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `estacion` VARCHAR(5) NULL,
  `fecha_creacion` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ultima_actualizacion` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcasetas`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `visitacionbd`.`guardaparques`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitacionbd`.`guardaparques` (
  `idguardaparques` TINYINT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NULL,
  `clave` VARCHAR(15) NULL,
  `fecha_creacion` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ultima_actualizacion` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idguardaparques`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `visitacionbd`.`nacionalidades`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitacionbd`.`nacionalidades` (
  `idnacionalidades` INT NOT NULL AUTO_INCREMENT,
  `nacion` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`idnacionalidades`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `visitacionbd`.`tipo_grafica`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitacionbd`.`tipo_grafica` (
  `idtipo_grafica` TINYINT NOT NULL AUTO_INCREMENT,
  `tipo` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`idtipo_grafica`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `visitacionbd`.`graficas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitacionbd`.`graficas` (
  `idgraficas` INT NOT NULL AUTO_INCREMENT,
  `idtipo_grafica` TINYINT NOT NULL,
  `mes_anio` VARCHAR(45) NOT NULL,
  `url` VARCHAR(45) NOT NULL,
  `fecha_creacion` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `actualizacion` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idgraficas`),
  INDEX `idtipo_grafica_idx` (`idtipo_grafica` ASC),
  CONSTRAINT `fk_idtipo_grafica`
    FOREIGN KEY (`idtipo_grafica`)
    REFERENCES `visitacionbd`.`tipo_grafica` (`idtipo_grafica`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `visitacionbd`.`visitacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitacionbd`.`visitacion` (
  `idvisitacion` INT NOT NULL AUTO_INCREMENT,
  `fecha` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idcasetas` TINYINT NOT NULL,
  `comentarios` VARCHAR(255) NULL,
  `actualizacion` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idvisitacion`),
  INDEX `fk_idcasetas_idx` (`idcasetas` ASC),
  CONSTRAINT `fk_idcasetas`
    FOREIGN KEY (`idcasetas`)
    REFERENCES `visitacionbd`.`casetas` (`idcasetas`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `visitacionbd`.`reportes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitacionbd`.`reportes` (
  `idreportes` INT NOT NULL AUTO_INCREMENT,
  `mes_anio` TIMESTAMP NOT NULL,
  `url` VARCHAR(45) NOT NULL,
  `fecha_creacion` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `actualizacion` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idreportes`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `visitacionbd`.`visitacion_guardaparques`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitacionbd`.`visitacion_guardaparques` (
  `idvisitacion_guardaparques` BIGINT NOT NULL AUTO_INCREMENT,
  `idvisitacion` INT NOT NULL,
  `idguardaparques` TINYINT NOT NULL,
  PRIMARY KEY (`idvisitacion_guardaparques`),
  INDEX `fk_idvisitacion_idx` (`idvisitacion` ASC),
  INDEX `fk_guardaparques_idx` (`idguardaparques` ASC),
  CONSTRAINT `fk_idvisitacion_guardaparques`
    FOREIGN KEY (`idvisitacion`)
    REFERENCES `visitacionbd`.`visitacion` (`idvisitacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_guardaparques`
    FOREIGN KEY (`idguardaparques`)
    REFERENCES `visitacionbd`.`guardaparques` (`idguardaparques`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `visitacionbd`.`visitacion_vehiculos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitacionbd`.`visitacion_vehiculos` (
  `idvehiculos` INT NOT NULL AUTO_INCREMENT,
  `idvisitacion` INT NOT NULL,
  `num_bici` INT NULL,
  `num_moto` INT NULL,
  `num_auto` INT NULL,
  `num_carga` INT NULL,
  PRIMARY KEY (`idvehiculos`),
  INDEX `fk_idvisitacion_idx` (`idvisitacion` ASC),
  CONSTRAINT `fk_idvisitacion_vehiculos`
    FOREIGN KEY (`idvisitacion`)
    REFERENCES `visitacionbd`.`visitacion` (`idvisitacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `visitacionbd`.`visitacion_visitantes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitacionbd`.`visitacion_visitantes` (
  `idvisitacion_visitantes` BIGINT NOT NULL AUTO_INCREMENT,
  `idvisitacion` INT NOT NULL,
  `visitantes` INT NULL,
  `idnacionalidades` INT NOT NULL,
  PRIMARY KEY (`idvisitacion_visitantes`),
  INDEX `fk_idnacionalidades_idx` (`idnacionalidades` ASC),
  INDEX `fk_idvisitacion_idx` (`idvisitacion` ASC),
  CONSTRAINT `fk_idnacionalidades`
    FOREIGN KEY (`idnacionalidades`)
    REFERENCES `visitacionbd`.`nacionalidades` (`idnacionalidades`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_idvisitacion_visitantes`
    FOREIGN KEY (`idvisitacion`)
    REFERENCES `visitacionbd`.`visitacion` (`idvisitacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `visitacionbd` ;

-- -----------------------------------------------------
-- Placeholder table for view `visitacionbd`.`vervisitacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitacionbd`.`vervisitacion` (`idvisitacion` INT, `idregistro` INT, `fecha` INT, `idcaseta` INT, `caseta` INT, `visitantes` INT, `nacionalidad` INT);

-- -----------------------------------------------------
-- Placeholder table for view `visitacionbd`.`vervehiculos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `visitacionbd`.`vervehiculos` (`idvisitacion` INT, `fecha` INT, `idcasetas` INT, `caseta` INT, `idvehiculos` INT, `bici` INT, `moto` INT, `auto` INT, `carga` INT);

-- -----------------------------------------------------
-- View `visitacionbd`.`vervisitacion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `visitacionbd`.`vervisitacion`;
USE `visitacionbd`;
CREATE  OR REPLACE VIEW `vervisitacion` AS
SELECT
  `visitacion_visitantes`.`idvisitacion_visitantes` AS idvisitacion,
  `visitacion`.`idvisitacion` AS idregistro,
  `visitacion`.`fecha` AS fecha,
  `casetas`.`idcasetas` AS idcaseta,
  `casetas`.`nombre` AS caseta,
  `visitacion_visitantes`.`visitantes` AS visitantes,
  `nacionalidades`.`nacion` AS nacionalidad
FROM
  `visitacion`
LEFT JOIN
  `visitacion_visitantes`
    ON
    `visitacion`.`idvisitacion` = `visitacion_visitantes`.`idvisitacion`
LEFT JOIN
   `nacionalidades`
     ON
     `nacionalidades`.`idnacionalidades` = `visitacion_visitantes`.`idnacionalidades`
LEFT JOIN
  `casetas`
    ON
    `casetas`.`idcasetas` = `visitacion`.`idcasetas`;

-- -----------------------------------------------------
-- View `visitacionbd`.`vervehiculos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `visitacionbd`.`vervehiculos`;
USE `visitacionbd`;
CREATE  OR REPLACE VIEW `vervehiculos` AS
SELECT
  `visitacion`.`idvisitacion` AS idvisitacion,
  `visitacion`.`fecha` AS fecha,
  `casetas`.`idcasetas` AS idcasetas,
  `casetas`.`nombre` AS caseta,
  `visitacion_vehiculos`.`idvehiculos` AS idvehiculos,
  `visitacion_vehiculos`.`num_bici` AS bici,
  `visitacion_vehiculos`.`num_moto` AS moto,
  `visitacion_vehiculos`.`num_auto` AS auto,
  `visitacion_vehiculos`.`num_carga` AS carga
FROM
  `visitacion`
LEFT JOIN
  `casetas`
    ON
    `casetas`.`idcasetas` = `visitacion`.`idcasetas`
LEFT JOIN
  `visitacion_vehiculos`
    ON
    `visitacion`.`idvisitacion` = `visitacion_vehiculos`.`idvisitacion`
 ORDER BY `visitacion`.`idvisitacion` ASC;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
