/*Visitación por años*/
SELECT
    `fecha`,
    MONTH(`fecha`) AS mes,
    SUM(`visitantes`) AS total
FROM
    `vervisitacion`
WHERE
    YEAR(`fecha`) = "2011"
GROUP BY
    MONTH(`fecha`)
ORDER BY
    `fecha` ASC


/*Visitación historica*/
SELECT
    YEAR(`fecha`) AS anio,
    MONTH(`fecha`) AS mes,
    SUM(`visitantes`) AS total
FROM
    `vervisitacion`
GROUP BY
    anio ASC,
    mes ASC


/*Visitacion de caseta por año*/
SELECT
	`caseta` AS caseta,
    YEAR(`fecha`) AS anio,
    SUM(`visitantes`) AS visitacion
FROM
    `vervisitacion`
WHERE `idcaseta` = "1"
GROUP BY
    caseta ASC,
    anio ASC


/*Visitacion de caseta total*/
SELECT
	`caseta` AS caseta,
    YEAR(`fecha`) AS anio,
    SUM(`visitantes`) AS visitacion
FROM
    `vervisitacion`
GROUP BY
    caseta ASC,
    anio ASC


/*Vehiculos de caseta por año*/
SELECT
	`caseta`,
    YEAR(`fecha`) AS anio,
    `bici`,
    `moto`,
    `auto`,
    `carga`
FROM
    `vervehiculos`
WHERE `idcasetas` = "5"
GROUP BY
    caseta ASC,
    anio ASC


/*Tipo de vehiculos totales*/
SELECT
    `caseta`,
    YEAR(`fecha`) AS anio,
    SUM(`bici`) AS bicis,
    SUM(`moto`) AS motos,
    SUM(`auto`) AS autos,
    SUM(`carga`) AS cargas
FROM
    `vervehiculos`
GROUP BY
    caseta ASC,
    anio ASC
ORDER BY
    `caseta` ASC



/*Principales nacionalidades*/
SELECT
    `nacionalidad`,
    SUM(`visitantes`) AS total
FROM
    `vervisitacion`
GROUP BY
    `nacionalidad`
ORDER BY
    `total`
DESC
LIMIT 10


/*Visitacion de determinada caseta y determinado año*/
SELECT
    SUM(`visitantes`) AS visitacion
FROM
    vervisitacion
WHERE
	YEAR(`fecha`) = "2010" AND
    `idcaseta` = "1"
GROUP BY
    caseta ASC
