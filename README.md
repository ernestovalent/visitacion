# Plataforma web para administrar la información de acceso y actividades del Complejo Sian Ka’an. - Proyecto de Residencia Profesional.

Por Ernesto Valentin Caamal Peech.

> El objetivo principal del proyecto de residencia profesional  
> es la implementación de una plataforma web para  
> administrar información del acceso y actividades  
> del Complejo Sian Ka’an como una medida de seguridad  
> para la conservación ambiental.



# Tabla de contenidos.

* Documentación
* Programacíon
* Lista de funciones
* Ultimos cambios

## Documentación.

El proyecto cuenta con dos tipos de documentación.

La documentación de la Plataforma Visitación está disponible en: [https://ernestovalent.gitbooks.io/manual-visitacion](https://ernestovalent.gitbooks.io/manual-visitacion)

Por otra parte la documentación del Proyecto de Residencia se encuentra actualmente en proceso de liberación. Los cambios se pueden proponer en [https://docs.google.com/document/d/1pi2b1Fn86PLHIs8YHHm2-48ohMqSoImh7h1-11D5Tuo/](https://docs.google.com/document/d/1pi2b1Fn86PLHIs8YHHm2-48ohMqSoImh7h1-11D5Tuo/edit?usp=sharing)

## Programación.

### Lenguajes.

La Plataforma se construye bajo el modelo de programación MVC orientado a objetos. Se utilizan diferentes lenguajes de programación y de hipermarcado.

* PHP
* HTML
* Javascript
* CSS3

### Dependencias del proyecto.

La plataforma Visitación hace uno de proyectos externos con los que mejora la experiencia del usuario y la integración de mejores códigos, los cuales crean dependencias externas que pueden ser descargados utilizando NPM. 

* Materializecss
* Jquery
* Chart.js
* Datatables.net
* Hammerjs
* Material Design Icons
* mpdf \(requiere composer\)

### Gestores.

Creando una estación de trabajo multiplataforma para el desarrollo del proyecto se necesita

* Atom
* Github
* Npm
* Workbench

## Lista de funciones

* Registrar flujo de visitantes a la Reserva.

* * Registra la fecha, la caseta, los guardaparques y visitación.
* Registrar vehículos entrantes a la Reserva.
  * Registra automóviles, bicicletas, motos y camiones de carga o vehículos pesados.
* Generar gráficas.
* Generar reportes.
* Permite realizar cambios a la configuración.
  * Permite modificar los datos de los 3 usuarios principales.
  * Permite administrar datos de Sian Ka'an, como las Casetas y los Guardaparques.
  * Permite consultar la documentación.

## Últimos cambios.

[**V0.0.1**](https://github.com/ernestovalent/visitacion/releases/tag/v0.0.1)** Inicial.-** Inicio del proyecto, construcción de modelo, vista controlador y navegación de estructura.

[**V0.0.5**](https://github.com/ernestovalent/visitacion/releases/tag/v0.0.5)** vistas.-** Creación de estructura de vistas posibles.

[**V0.1**](https://github.com/ernestovalent/visitacion/releases/tag/v0.1)** Navegacion y DB.- **Navegación entre vistas funcional e integración de base de datos.

[**v0.1.3**](https://github.com/ernestovalent/visitacion/releases/tag/v0.1.3)** Jquery y Charts.-** Integración de Jquery en diferentes vistas y creación inicial del generador de gráficas.

[**v0.2**](https://github.com/ernestovalent/visitacion/releases/tag/v0.2)** reportes en PDF.-** Integración de mpdf y posibilidades de generar PDF \(Soporte solo para Windows\).

[**v0.5**](https://github.com/ernestovalent/visitacion/releases/tag/v0.5)** Prelanzamiento.-** Funcionamiento de todas las vistas básicas y presentación del proyecto. Prelanzamiento antes de implementación.

#### Siguientes versiones.

Se espera que en las siguientes versiones se puedan mejorar varias funciones actuales y crear nuevas funciones.

Se describen a continuación:

* Agregar Validadores de formularios local y servidor.
* Agregar función de administración de nacionalidades.
* Agregar función de respaldo de base de datos.
* Agregar función de descargar base de datos en Excel.
* Agregar función de editar visitación.
* Agregar función de guardar gráficas en servidor.
* Agregar vista de galería de gráficas generadas.
* Agregar gráficas a los reportes.
* Modificar creador de cuentas de usuario.





