<?php
class MainController{
  public function plantillaInicio(){
    include "views/index.php";
  }

  //Controlador de enlaces
  public function enlacesPaginasController(){
    if (isset($_GET["v"])) {
      $enlacesController = $_GET["v"];
    }elseif (isset($_GET["panel"])) {
      $enlacesController = $_GET["panel"];
    }elseif (isset($_GET["configurar"])) {
      $enlacesController = $_GET["configurar"];
    }else {
      $enlacesController = "inicio";
    }
    $respuesta = Navegacion::EnlacesPaginasModelos($enlacesController);
    include $respuesta;
  }

  //Inicio de Sesion
  public function CRUDIngresoPlataformaController(){
    if (isset($_POST['usuario']) && $_POST['usuario']!="") {
      $datosController = array('usuario' => $_POST['usuario'],
                                'password' => $_POST['password'] );
      $respuesta = CRUD::CRUDIngresoPlataformaModel($datosController, "usuarios");
      if ($respuesta['correo'] == $_POST['usuario'] && $respuesta['contra'] == $_POST['password']) {
        session_start();
        $_SESSION["validar"] = true;
        $_SESSION["nombre"] = $respuesta['nombre'];
        header("location:?v=panel");
      }else {
        header("location:?v=iniciar");
      }
    }
  }

  //Registro de Casetas
  public function CRUDRegistroCasetasController(){
    if (isset($_POST['nuevaCasetaNombre'])) {
      $datosController = array('estacion' => $_POST['nuevaCasetaEstacion'],
        'nombre' => $_POST['nuevaCasetaNombre'] );
      $respuesta = CRUD::CRUDRegistroCasetasModel($datosController, "casetas");
      if ($respuesta == true) {
        header("location:index.php?configurar=siankaan&sql=ok");
      }else {
        header("location:index.php?configurar=siankaan&sql=error");
      }

    }
  }

  //Registro de Guardaparques
  public function CRUDRegistroGuardaparquesController(){
    if (isset($_POST['nuevoGuardaparqueNombre']) && isset($_POST['nuevoGuardaparqueApellido'])) {
      $datosController = array('nombre' => $_POST['nuevoGuardaparqueNombre'],
        'apellido' => $_POST['nuevoGuardaparqueApellido'],
        'clave' => $_POST['nuevoGuardaparqueClave'] );
      $respuesta = CRUD::CRUDRegistroGuardaparquesModel($datosController, "guardaparques");
      if ($respuesta == true) {
        header("location:index.php?configurar=siankaan&sql=ok");
      }else {
        header("location:index.php?configurar=siankaan&sql=error");
      }
    }
  }

  //Registro de visitacion
  public function CRUDRegistroVisitacionController(){
    if (isset($_POST['fecha']) && isset($_POST['caseta'])) {
      $datosVisitacion = array('fecha' => $_POST['fecha'], 'caseta' => $_POST['caseta'], 'comentarios' => $_POST['comentarios']);
      $registroVisitacion = CRUD::CRUDRegistroVisitacionModel($datosVisitacion, "visitacion");
      if ($registroVisitacion == true) {
        //echo "Fecha y caseta<br>";
        $ultimoIdVisitacion = CRUD::ultimoID("idvisitacion","visitacion");
        if (isset($_POST['visitante']) && isset($_POST['nacion'])) {
          $variableVisitante = $_POST['visitante'];
          $variableNacion = $_POST['nacion'];
          foreach ($variableVisitante as $index => $value) {
            $registroVisitantes = CRUD::CRUDRegistroVisitantesModel("visitacion_visitantes", $ultimoIdVisitacion, $value, $variableNacion[$index]);
          }
          if ($registroVisitantes == true) {
            //echo "Visitante y nacion<br>";
            if (isset($_POST['guardaparques'])) {
              $variableGuardaparques = $_POST['guardaparques'];
              foreach ($variableGuardaparques as $value) {
                $registroGuardaparques = CRUD::CRUDRegistroVisitacionGuardaparquesModel("visitacion_guardaparques", $ultimoIdVisitacion, $value);
              }
              if ($registroGuardaparques == true) {
                //echo "Guardaparques <br>";
                if (isset($_POST['bicicletas']) || isset($_POST['motos']) || isset($_POST['carros']) || isset($_POST['carga'])) {
                  $datosVehiculos = array('bici' => $_POST['bicicletas'], 'moto' => $_POST['motos'], 'auto' => $_POST['carros'], 'carga' => $_POST['carga']);
                  $registroVehiculos = CRUD::CRUDRegistroVehiculosModel("visitacion_vehiculos", $ultimoIdVisitacion, $datosVehiculos);
                  if ($registroVehiculos == true) {
                    //echo "Vehiculos <br>";
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  //Vista de visitacion de entre un rango de fechas.
  public function CRUDVistaVerVisitacionController(){
    if (isset($_POST['fecha-final'])) {
      $datosController = array('inicio' => $_POST['fecha-inicial'], 'fin' => $_POST['fecha-final'] );
      $respuesta = CRUD::CRUDVistaVerVisitacionModel($datosController, "vervisitacion");
      return $respuesta;
    }
  }

  public function CRUDVistaVisitacionIdController($id){
    $respuesta = CRUD::CRUDVistaVisitacionIdModel($id);
    return $respuesta;
  }

  //Vista para ver los años visitacion
  public function CRUDVistaVisitacionYearController(){
    $respuesta = CRUD::CRUDVistaVisitacionYearModel();
    return $respuesta;
  }

  public function CRUDVistaVisitacionYearMinMaxController(){
    $respuesta = CRUD::CRUDVistaVisitacionYearMinMaxModel();
    return $respuesta;
  }

  //Vista de usuarios
  public function CRUDVistaUsuariosController($id){
    $respuesta = CRUD::CRUDVistaUsuariosModel("usuarios", $id);
    return $respuesta;
  }

  //Vista de casetas en Administrar datos de Sian Kaan
  public function CRUDVistaCasetasController(){
    $respuesta = CRUD::CRUDVistaCasetasModel("casetas");
    return $respuesta;
  }

  //Vista de guardaparques en Administrar datos de Sian Kaan
  public function CRUDVistaGuardaparquesController(){
    $respuesta = CRUD::CRUDVistaGuardaparquesModel("guardaparques");
    return $respuesta;
  }

  public function CRUDVistaGuardaparquesVisitacionController($id){
    $respuesta = CRUD::CRUDVistaGuardaparquesVisitacionModel($id);
    return $respuesta;
  }

  //Vista de nacionalidades en Registro de Visitacion
  public function CRUDVistaNacionalidadesController(){
    $respuesta = CRUD::CRUDVistaNacionalidadesModel("nacionalidades");
    return $respuesta;
  }

  //Actualizar USUARIOS en cuenta
  public function CRUDActualizarUsuariosController(){
    if (isset($_POST['adminNombre'])) {
      $datosController = array('nombre' => $_POST['adminNombre'],
                                'apellido' => $_POST['adminApellido'],
                                'correo' => $_POST['adminCorreoUno'],
                                'password' => $_POST['adminContraUno']);
      $respuesta = CRUD::CRUDActualizarUsuariosModel($datosController, "usuarios", "2");
      if ($respuesta == true) {
        header("location:?configurar=cuenta");
      }
    }if (isset($_POST['subNombre'])) {
      $datosController = array('nombre' => $_POST['subNombre'],
                                'apellido' => $_POST['subApellido'],
                                'correo' => $_POST['SubCorreo'],
                                'password' => $_POST['subContra']);
      $respuesta = CRUD::CRUDActualizarUsuariosModel($datosController, "usuarios", "4");
      if ($respuesta == true) {
        header("location:?configurar=cuenta");
      }
    }if (isset($_POST['dirNombre'])) {
      $datosController = array('nombre' => $_POST['dirNombre'],
                                'apellido' => $_POST['dirApellido'],
                                'correo' => $_POST['dirCorreo'],
                                'password' => $_POST['dirContra']);
      $respuesta = CRUD::CRUDActualizarUsuariosModel($datosController, "usuarios", "3");
      if ($respuesta == true) {
        header("location:?configurar=cuenta");
      }
    }
  }

  //Actualizar casetas en datos de Sian Kaan
  public function CRUDActualizarCasetasController(){
    if (isset($_POST['caseta'])) {
      $datosController = array('idcaseta' => $_POST['idcaseta'],
        'estacion' => $_POST['estacion'],
        'caseta' => $_POST['caseta']);
      $respuesta = CRUD::CRUDActualizarCasetasModel($datosController, "casetas");
      if ($respuesta == true) {
        header("location:?configurar=siankaan");
      }
    }
  }

  //Actualizar guardaparque de datos de Sian Ka'an
  public function CRUDActualizarGuardaparqueController(){
    if (isset($_POST['guardaparqueNombre'])) {
      $datosController = array('id' => $_POST['idguardaparque'],
        'clave' => $_POST['clave'],
        'nombre' => $_POST['guardaparqueNombre'],
        'apellido' => $_POST['guardaparqueApellido']);
      $respuesta = CRUD::CRUDActualizarGuardaparqueModel($datosController, "guardaparques");
      if ($respuesta == true) {
        header("location:?configurar=siankaan");
      }
    }
  }

  //Actualizar visitacion por ID
  public function CRUDActualizarVisitacionIDController($idVisitacion, $idVisitantes){
    //echo "ID visitacion: $idVisitacion, ID visitantes: $idVisitantes";
    if (isset($_POST['fecha']) || isset($_POST['caseta'])) {
      $datosVisitacion = array('fecha' => $_POST['fecha'], 'caseta' => $_POST['caseta']);
      $actualizarVisitacion = CRUD::CRUDActualizarVisitacionModel(1, "visitacion", $datosVisitacion, $idVisitacion);
      if ($actualizarVisitacion == true) {
        echo "<script>alert('Registro de visitacion exitoso');</script>";
        header("location:?panel=editar-visitacion-id&id=$idVisitantes");
      }
    }
    if (isset($_POST['visitantes']) && isset($_POST['nacionalidad'])) {
      $datosVisitantes = array('visitantes' => $_POST['visitantes'], 'nacionalidad' => $_POST['nacionalidad']);
      $actualizarVisitantes = CRUD::CRUDActualizarVisitacionModel(2, "visitacion_visitantes", $datosVisitantes, $idVisitantes);
      if ($actualizarVisitantes == true) {
        echo "<script>alert('Registro de visitantes exitoso');</script>";
        header("location:?panel=editar-visitacion-id&id=$idVisitantes");
      }
    }
  }

  //Borrar Caseta registradas
  public function CRUDBorrarCasetaController(){
    if (isset($_POST['borrarCaseta'])) {
      $datosController = array('id' => $_POST['idcaseta']);
      $respuesta = CRUD::CRUDBorrarCasetaModel($datosController, "casetas");
      if ($respuesta == true) {
        header("location:?configurar=siankaan");
      }else {
        echo '<script>alert("Esta caseta está en uso, no puede eliminarse.");</script>';
      }
    }
  }

  //Borrar Guardaparque registrado
  public function CRUDBorrarGuardaparqueController(){
    if (isset($_POST['borrarGuardaparque'])) {
      $datosController = array('id' => $_POST['idguardaparque']);
      $respuesta = CRUD::CRUDBorrarGuardaparqueModel($datosController, "guardaparques");
      if ($respuesta == true) {
        header("location:?configurar=siankaan");
      }else{
        echo '<script>alert("El nombre de este guardaparque está en uso, no puede eliminarse.");</script>';
      }
    }
  }


  //ajax
  public function ajaxVisitacionAniosController($anio){
    $respuesta = CRUD::ajaxVisitacionAniosModel("vervisitacion", $anio);
    return $respuesta;
  }

  public function ajaxVisitacionHistoricaController(){
    $respuesta = CRUD::ajaxVisitacionHistoricaModel("vervisitacion");
    return $respuesta;
  }

  public function ajaxVisitacionCasetaAniosController($caseta){
    $respuesta = CRUD::ajaxVisitacionCasetaAniosModel("vervisitacion", $caseta);
    return $respuesta;
  }

  public function ajaxVisitacionCasetasHistoricoController(){
    $respuesta = CRUD::ajaxVisitacionCasetasHistoricoModel("vervisitacion");
    return $respuesta;
  }

  public function ajaxVehiculosCasetaAnioController($caseta){
    $respuesta = CRUD::ajaxVehiculosCasetaAnioModel("vervehiculos", $caseta);
    return $respuesta;
  }

  public function ajaxVehiculosTotalesController(){
    $respuesta = CRUD::ajaxVehiculosTotalesModel("vervehiculos");
    return $respuesta;
  }

  public function RVisitacionHistoricaController($anio, $mes){
    $respuesta = CRUD::RVisitacionHistoricaModel($anio, $mes);
    return $respuesta;
  }

  public function RVisitacionCasetaController($anio, $caseta){
    $respuesta = CRUD::RVisitacionCasetaModel($anio, $caseta);
    return $respuesta;
  }

  public function RVisitacionNacionalidadesController(){
    $respuesta = CRUD::RVisitacionNacionalidadesModel();
    return $respuesta;
  }

  public function RVisitacionNacionalidadesAnioController($anio){
    $respuesta = CRUD::RVisitacionNacionalidadesAnioModel($anio);
    return $respuesta;
  }

}
?>
