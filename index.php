<?php
//require_once Estable que el controlador y modelos son obligatorios.
require_once "controllers/controller.php";
require_once "models/navegacion.php";
require_once "models/crud.php";
if (!file_exists("node_modules/")) {
  echo "Error en la instalación, faltan agregar dependencias de node: Ejecutar npm install en linea de comandos.";
}else {
//Si existe node, entonces llama a la plantilla (vista) usando el controlador.
$main = new MainController();
$main -> plantillaInicio();
}
?>
